﻿# Arquitetura hexagonal em Delphi

Este projeto demonstra a implementação de um cadastro de clientes e seus respectivos contatos e telefones através dos
princípios da arquitetura hexagonal.

O objetivo é disponibilizar uma API REST para cadastrar, alterar, excluir, buscar e consultar clientes em bancos de
dados Postgres, MySQL e MongoDB.

## Sobre a arquitetura hexagonal

A arquitetura hexagonal é um padrão de estruturação de aplicação que prevê que:

- A aplicação deve ser igualmente controlável por usuários, outras aplicações ou testes automatizados. Para as regras de
  negócio, não há diferença se a aplicação é utilizada por um usuário através de uma UI ou por uma API REST;
- As regras de negócio devem ser implementadas de forma isolada independente de questões de infraestrutura como bancos
  de dados, frameworks ou outras aplicações. Não faz diferença se os dados são armazenados em um banco relacional, em um
  banco NoSQL ou em uma aplicação externa;
- A troca de componentes de infraestrutura como servidores de bancos de dados e frameworks ou até mesmo a adaptação à
  novas UIs deve ser possível sem ajustes nas regras de negócio.

O isolamento das regras de negócio do mundo externo é atingido através de portas conforme mostrado na imagem abaixo:

![Arquitetura hexagonal](docs/arquitetura.png)

As regras de negócio ("application") compõem o *core* da arquitetura. No *core* também se encontram as interfaces
("ports") que são responsáveis por conectá-lo à UIs, APIs REST, bancos de dados, etc. Existem dois tipos de porta: as de
entrada que controlam o *core* e as de saída que são controladas por ele.

A conexão com os componentes de infraestrutura é feita através dos adaptadores. Os adaptadores definem, por exemplo,
controladores de APIs REST que usam as portas de entrada para disparar ações no *core*. Do outro lado, o *core* pode
buscar ou gravar informações em bancos de dados através de adaptadores de persistência que implementam as portas de
saída.

## Requisitos

- Delphi 11+ (Alexandria) e [DelphiMVC](https://github.com/danieleteti/delphimvcframework);
- `git-bash`, `curl` e [jq](https://jqlang.github.io/jq) para execução de APIs.

## Organização

A aplicação está estruturada com os seguintes módulos:

```
src
├── adaptadores
├── aplicacao
├── bootstrap
├── infraestrutura
└── modelo
```

- **infraestrutura**: Contém classes para manipulação de configurações e acesso a bancos de dados;
- **modelo**: Contém as classes que representam o cadastro de clientes. Foram criadas classes adicionais para os
  atributos que contém algum tipo de formatação e/ou regra;
- **aplicacao**: Contém as portas de entrada e saída e os serviços que implementam as portas de entrada. Junto com o
  modelo, forma o *core* da aplicação;
- **adaptadores**: Contém os adaptadores REST e de persistência;
- **bootstrap**: Contém a aplicação em si e é responsável por disponibilizar a API REST.

## Execução

Abrir `hexagonal-architecture-delphi.groupproj` e compilar todos os projetos. Os binários serão gerados na pasta `bin`.

Copiar os arquivos da pasta `samples` para `bin`.

Criar os bancos de dados Postgres e MySQL e executar os scripts `bd-pg.sql` e `bd-mysql.sql`.

Criar o arquivo `bin\config.properties` com as configurações da aplicação:

```
# Persistência
persistencia=mysql|postgres|mongodb
## MySQL
persistencia.mysql.servidor=servidor
persistencia.mysql.porta=porta
persistencia.mysql.bancoDeDados=bancoDeDados
persistencia.mysql.usuario=usuario
persistencia.mysql.senha=senha
## Postgres
persistencia.postgres.servidor=servidor
persistencia.postgres.porta=porta
persistencia.postgres.bancoDeDados=bancoDeDados
persistencia.postgres.usuario=usuario
persistencia.postgres.senha=senha
## MongoDB
persistencia.mongodb.servidor=servidor
persistencia.mongodb.porta=porta
persistencia.mongodb.bancoDeDados=bancoDeDados
persistencia.mongodb.usuario=usuario
persistencia.mongodb.senha=senha
```

Executar `bin\Bootstrap.exe`. A aplicação estará rodando na porta **8080**.

## Exemplos

Seguem as chamadas da API REST via linha de comando com `curl` e `jq`. É preciso executá-las através do bash do Git.

Inserir cliente:

```bash
curl --request POST \
  --url http://localhost:8080/cliente \
  --header 'Content-Type: application/json' \
  --data '{
    "NomeFantasia": "Rafael C. Luiz",
    "RazaoSocial": "Rafael C. Luiz",
    "CPFJ": "88614664001",
    "Contatos": [
      {
        "Nome": "Contato 1",
        "Telefones": [
          {
            "Descricao": "1",
            "Numero": "19989892121"
          }
        ]
      },
      {
        "Nome": "Contato 2",
        "Telefones": [
          {
            "Descricao": "1",
            "Numero": "19989892121"
          }
        ]
      }
    ]
  }' | jq
```

Alterar cliente:

```bash
curl --request PUT \
  --url http://localhost:8080/cliente \
  --header 'Content-Type: application/json' \
  --data '{
    "NomeFantasia": "Rafael C. Luiz",
    "RazaoSocial": "Rafael C. Luiz",
    "CPFJ": "88614664001",
    "Contatos": [
      {
        "Nome": "Contato 1",
        "Telefones": [
          {
            "Descricao": "1",
            "Numero": "19989892121"
          }
        ]
      },
      {
        "Nome": "Contato 2",
        "Telefones": [
          {
            "Descricao": "1",
            "Numero": "19989892121"
          }
        ]
      }
    ]
  }' | jq
```

Excluir cliente:

```bash
curl --request DELETE --url http://localhost:8080/cliente/88614664001 | jq
```

Buscar cliente:

```bash
curl --request GET --url http://localhost:8080/cliente/88614664001 | jq
```

Consultar clientes:

```bash
curl --request GET --url http://localhost:8080/cliente?nomeFantasia=Rafael | jq
```
