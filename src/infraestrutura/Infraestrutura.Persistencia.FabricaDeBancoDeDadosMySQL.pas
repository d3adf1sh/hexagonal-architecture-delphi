unit Infraestrutura.Persistencia.FabricaDeBancoDeDadosMySQL;

interface

uses
  System.SysUtils,
  System.Generics.Collections,
  Infraestrutura.Persistencia.BancoDeDados,
  Infraestrutura.Persistencia.FabricaDeBancoDeDados;

type
  //TFabricaDeBancoDeDadosMySQL
  TFabricaDeBancoDeDadosMySQL = class sealed(TInterfacedObject, IFabricaDeBancoDeDados)
  private
    FParametros: TDictionary<string, string>;
  public
    constructor Create;
    destructor Destroy; override;
    function Criar: IBancoDeDados;
  end;

implementation

uses
  Infraestrutura.Parametros,
  Infraestrutura.Parametros.Constantes,
  Infraestrutura.Persistencia.BancoDeDadosMySQL;

{ TFabricaDeBancoDeDadosMySQL }

constructor TFabricaDeBancoDeDadosMySQL.Create;
begin
  FParametros := TDictionary<string, string>.Create;
  FParametros.Add(CServidor, TParametros.Instancia.Ler([CPersistencia, CMySQL, CServidor]));
  FParametros.Add(CPorta, TParametros.Instancia.Ler([CPersistencia, CMySQL, CPorta]));
  FParametros.Add(CBancoDeDados, TParametros.Instancia.Ler([CPersistencia, CMySQL, CBancoDeDados]));
  FParametros.Add(CUsuario, TParametros.Instancia.Ler([CPersistencia, CMySQL, CUsuario]));
  FParametros.Add(CSenha, TParametros.Instancia.Ler([CPersistencia, CMySQL, CSenha]));
end;

destructor TFabricaDeBancoDeDadosMySQL.Destroy;
begin
  FreeAndNil(FParametros);
  inherited;
end;

function TFabricaDeBancoDeDadosMySQL.Criar: IBancoDeDados;
begin
  Result := TBancoDeDadosMySQL.Create(FParametros);
  Result.Conectar;
end;

end.
