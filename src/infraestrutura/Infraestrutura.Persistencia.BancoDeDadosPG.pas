unit Infraestrutura.Persistencia.BancoDeDadosPG;

interface

uses
  System.Classes,
  System.Generics.Collections,
  Data.DB,
  FireDAC.Comp.Client,
  Infraestrutura.Persistencia.BancoDeDados;

type
  //TBancoDeDadosPG
  TBancoDeDadosPG = class sealed(TInterfacedObject, IBancoDedados)
  private
    FParametros: TDictionary<string, string>;
    FConexao: TFDConnection;
    FTransacoes: Integer;
    procedure ChecarConexao;
  public
    constructor Create(const AParametros: TDictionary<string, string>);
    destructor Destroy; override;
    procedure Conectar;
    procedure Desconectar;
    function Conectado: Boolean;
    function Consultar(const ASQL: string): TDataSet;
    function Executar(const ASQL: string): Integer;
    procedure IniciarTransacao;
    function EmTransacao: Boolean;
    procedure ConfirmarTransacao;
    procedure CancelarTransacao;
  end;

implementation

uses
  System.SysUtils,
  FireDAC.Stan.Def,
  FireDAC.Stan.Async,
  FireDAC.Stan.Param,
  FireDAC.Phys.PG,
  FireDAC.Phys.PGDef,
  FireDAC.DApt,
  FireDAC.UI.Intf,
  FireDAC.Comp.UI,
  FireDAC.VCLUI.Wait,
  Infraestrutura.Parametros.Constantes,
  Infraestrutura.Persistencia.BancoDeDados.Erros;

{ TBancoDeDadosPG }

constructor TBancoDeDadosPG.Create(const AParametros: TDictionary<string, string>);
begin
  FParametros := TDictionary<string, string>.Create(AParametros);
end;

destructor TBancoDeDadosPG.Destroy;
begin
  if Conectado then
    Desconectar;
  FreeAndNil(FParametros);
  inherited;
end;

procedure TBancoDeDadosPG.ChecarConexao;
begin
  if not Conectado then
    raise EBancoDeDadosNaoConectado.Create('Banco de dados n�o conectado.');
end;

procedure TBancoDeDadosPG.Conectar;
var
  lParams: TFDPhysPGConnectionDefParams;
begin
  if Conectado then
    raise EBancoDeDadosJaConectado.Create('Banco de dados j� conectado.');
  FConexao := TFDConnection.Create(nil);
  try
    FConexao.DriverName := 'PG';
    lParams := TFDPhysPGConnectionDefParams(FConexao.Params);
    lParams.Server := FParametros[CServidor];
    lParams.Port := StrToIntDef(FParametros[CPorta], 0);
    lParams.Database := FParametros[CBancoDeDados];
    lParams.UserName := FParametros[CUsuario];
    lParams.Password := FParametros[CSenha];
    FConexao.Open;
  except;
    FreeAndNil(FConexao);
    raise;
  end;
end;

procedure TBancoDeDadosPG.Desconectar;
begin
  ChecarConexao;
  CancelarTransacao;
  try
    FConexao.Close;
  finally
    FreeAndNil(FConexao);
  end;
end;

function TBancoDeDadosPG.Conectado: Boolean;
begin
  Result := Assigned(FConexao) and FConexao.Connected;
end;

function TBancoDeDadosPG.Consultar(const ASQL: string): TDataSet;
var
  lConsulta: TFDQuery;
begin
  ChecarConexao;
  lConsulta := TFDQuery.Create(nil);
  try
    lConsulta.Connection := FConexao;
    lConsulta.SQL.Text := ASQL;
    lConsulta.Open;
    Result := lConsulta;
  except
    FreeAndNil(lConsulta);
    raise;
  end;
end;

function TBancoDeDadosPG.Executar(const ASQL: string): Integer;
var
  lComando: TFDQuery;
  lID: TFDParam;
begin
  ChecarConexao;
  lComando := TFDQuery.Create(nil);
  try
    lComando.Connection := FConexao;
    lComando.SQL.Text := ASQL;
    lID := lComando.FindParam('ID');
    if Assigned(lID) then
      lID.ParamType := ptOutput;
    lComando.ExecSQL;
    if Assigned(lID) then
      Result := lID.AsInteger
    else
      Result := 0;
  finally
    FreeAndNil(lComando);
  end;
end;

procedure TBancoDeDadosPG.IniciarTransacao;
begin
  ChecarConexao;
  if FTransacoes = 0 then
    FConexao.StartTransaction;
  Inc(FTransacoes);
end;

function TBancoDeDadosPG.EmTransacao: Boolean;
begin
  Result := FConexao.InTransaction;
end;

procedure TBancoDeDadosPG.ConfirmarTransacao;
begin
  if FTransacoes > 0 then
  begin
    Dec(FTransacoes);
    if FTransacoes = 0 then
      FConexao.Commit;
  end;
end;

procedure TBancoDeDadosPG.CancelarTransacao;
begin
  if FTransacoes > 0 then
  begin
    Dec(FTransacoes);
    if FTransacoes = 0 then
      FConexao.Rollback;
  end;
end;

end.
