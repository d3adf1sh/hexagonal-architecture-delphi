unit Infraestrutura.Persistencia.FabricaDeBancoDeDados;

interface

uses
  Infraestrutura.Persistencia.BancoDeDados;

type
  //IFabricaDeBancoDeDados
  IFabricaDeBancoDeDados = interface
    ['{94435742-5A9C-4A0A-8184-CF157FF5D6DB}']
    function Criar: IBancoDeDados;
  end;

implementation

end.
