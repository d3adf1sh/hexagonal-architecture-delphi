unit Infraestrutura.Persistencia.BancoDeDados.Erros;

interface

uses
  System.SysUtils;

type
  //EBancoDeDadosInvalido
  EBancoDeDadosInvalido = class(Exception);

  //EBancoDeDadosJaConectado
  EBancoDeDadosJaConectado = class(Exception);

  //EBancoDeDadosNaoConectado
  EBancoDeDadosNaoConectado = class(Exception);

implementation

end.
