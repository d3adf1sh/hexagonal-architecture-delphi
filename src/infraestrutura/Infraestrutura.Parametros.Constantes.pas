unit Infraestrutura.Parametros.Constantes;

interface

const
  CPersistencia = 'persistencia';
  CMySQL = 'mysql';
  CPG = 'postgres';
  CMongoDB = 'mongodb';
  CServidor = 'servidor';
  CPorta  = 'porta';
  CBancoDeDados = 'bancoDeDados';
  CUsuario = 'usuario';
  CSenha = 'senha';

implementation

end.
