unit Infraestrutura.Persistencia.BancoDeDados;

interface

uses
  System.SysUtils,
  Data.DB;

type
  //IBancoDeDados
  IBancoDeDados = interface
    procedure Conectar;
    procedure Desconectar;
    function Conectado: Boolean;
    function Consultar(const ASQL: string): TDataSet;
    function Executar(const ASQL: string): Integer;
    procedure IniciarTransacao;
    function EmTransacao: Boolean;
    procedure ConfirmarTransacao;
    procedure CancelarTransacao;
  end;

implementation

end.
