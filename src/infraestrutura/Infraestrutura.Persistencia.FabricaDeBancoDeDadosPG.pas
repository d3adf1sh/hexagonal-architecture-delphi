unit Infraestrutura.Persistencia.FabricaDeBancoDeDadosPG;

interface

uses
  System.SysUtils,
  System.Generics.Collections,
  Infraestrutura.Persistencia.BancoDeDados,
  Infraestrutura.Persistencia.FabricaDeBancoDeDados;

type
  //TFabricaDeBancoDeDadosPG
  TFabricaDeBancoDeDadosPG = class sealed(TInterfacedObject, IFabricaDeBancoDeDados)
  private
    FParametros: TDictionary<string, string>;
  public
    constructor Create;
    destructor Destroy; override;
    function Criar: IBancoDeDados;
  end;

implementation

uses
  Infraestrutura.Parametros,
  Infraestrutura.Parametros.Constantes,
  Infraestrutura.Persistencia.BancoDeDadosPG;

{ TFabricaDeBancoDeDadosPG }

constructor TFabricaDeBancoDeDadosPG.Create;
begin
  FParametros := TDictionary<string, string>.Create;
  FParametros.Add(CServidor, TParametros.Instancia.Ler([CPersistencia, CPG, CServidor]));
  FParametros.Add(CPorta, TParametros.Instancia.Ler([CPersistencia, CPG, CPorta]));
  FParametros.Add(CBancoDeDados, TParametros.Instancia.Ler([CPersistencia, CPG, CBancoDeDados]));
  FParametros.Add(CUsuario, TParametros.Instancia.Ler([CPersistencia, CPG, CUsuario]));
  FParametros.Add(CSenha, TParametros.Instancia.Ler([CPersistencia, CPG, CSenha]));
end;

destructor TFabricaDeBancoDeDadosPG.Destroy;
begin
  FreeAndNil(FParametros);
  inherited;
end;

function TFabricaDeBancoDeDadosPG.Criar: IBancoDeDados;
begin
  Result := TBancoDeDadosPG.Create(FParametros);
  Result.Conectar;
end;

end.
