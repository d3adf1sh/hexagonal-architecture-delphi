unit Infraestrutura.Persistencia.BancoDeDadosMySQL;

interface

uses
  System.Classes,
  System.Generics.Collections,
  Data.DB,
  FireDAC.Comp.Client,
  Infraestrutura.Persistencia.BancoDeDados;

type
  //TBancoDeDadosMySQL
  TBancoDeDadosMySQL = class sealed(TInterfacedObject, IBancoDedados)
  private
    FParametros: TDictionary<string, string>;
    FConexao: TFDConnection;
    FTransacoes: Integer;
    procedure ChecarConexao;
  public
    constructor Create(const AParametros: TDictionary<string, string>);
    destructor Destroy; override;
    procedure Conectar;
    procedure Desconectar;
    function Conectado: Boolean;
    function Consultar(const ASQL: string): TDataSet;
    function Executar(const ASQL: string): Integer;
    procedure IniciarTransacao;
    function EmTransacao: Boolean;
    procedure ConfirmarTransacao;
    procedure CancelarTransacao;
  end;

implementation

uses
  System.SysUtils,
  System.StrUtils,
  System.Variants,
  FireDAC.Stan.Def,
  FireDAC.Stan.Async,
  FireDAC.Stan.Param,
  FireDAC.Phys.MySQL,
  FireDAC.Phys.MySQLDef,
  FireDAC.DApt,
  FireDAC.UI.Intf,
  FireDAC.Comp.UI,
  FireDAC.VCLUI.Wait,
  Infraestrutura.Parametros.Constantes,
  Infraestrutura.Persistencia.BancoDeDados.Erros;

{ TBancoDeDadosMySQL }

constructor TBancoDeDadosMySQL.Create(const AParametros: TDictionary<string, string>);
begin
  FParametros := TDictionary<string, string>.Create(AParametros);
end;

destructor TBancoDeDadosMySQL.Destroy;
begin
  if Conectado then
    Desconectar;
  FreeAndNil(FParametros);
  inherited;
end;

procedure TBancoDeDadosMySQL.ChecarConexao;
begin
  if not Conectado then
    raise EBancoDeDadosNaoConectado.Create('Banco de dados n�o conectado.');
end;

procedure TBancoDeDadosMySQL.Conectar;
var
  lParams: TFDPhysMySQLConnectionDefParams;
begin
  if Conectado then
    raise EBancoDeDadosJaConectado.Create('Banco de dados j� conectado.');
  FConexao := TFDConnection.Create(nil);
  try
    FConexao.DriverName := 'MySQL';
    lParams := TFDPhysMySQLConnectionDefParams(FConexao.Params);
    lParams.Server := FParametros[CServidor];
    lParams.Port := StrToIntDef(FParametros[CPorta], 0);
    lParams.Database := FParametros[CBancoDeDados];
    lParams.UserName := FParametros[CUsuario];
    lParams.Password := FParametros[CSenha];
    FConexao.Open;
  except;
    FreeAndNil(FConexao);
    raise;
  end;
end;

procedure TBancoDeDadosMySQL.Desconectar;
begin
  ChecarConexao;
  CancelarTransacao;
  try
    FConexao.Close;
  finally
    FreeAndNil(FConexao);
  end;
end;

function TBancoDeDadosMySQL.Conectado: Boolean;
begin
  Result := Assigned(FConexao) and FConexao.Connected;
end;

function TBancoDeDadosMySQL.Consultar(const ASQL: string): TDataSet;
var
  lConsulta: TFDQuery;
begin
  ChecarConexao;
  lConsulta := TFDQuery.Create(nil);
  try
    lConsulta.Connection := FConexao;
    lConsulta.SQL.Text := ASQL;
    lConsulta.Open;
    Result := lConsulta;
  except
    FreeAndNil(lConsulta);
    raise;
  end;
end;

function TBancoDeDadosMySQL.Executar(const ASQL: string): Integer;
var
  lComando: TFDQuery;
  lID: Variant;
begin
  ChecarConexao;
  lComando := TFDQuery.Create(nil);
  try
    lComando.Connection := FConexao;
    lComando.SQL.Text := ASQL;
    lComando.ExecSQL;
    Result := 0;
    if StartsText('INSERT', ASQL) then
    begin
      lID := FConexao.GetLastAutoGenValue('');
      if VarIsNumeric(lID) then
        Result := lID;
    end;
  finally
    FreeAndNil(lComando);
  end;
end;

procedure TBancoDeDadosMySQL.IniciarTransacao;
begin
  ChecarConexao;
  if FTransacoes = 0 then
    FConexao.StartTransaction;
  Inc(FTransacoes);
end;

function TBancoDeDadosMySQL.EmTransacao: Boolean;
begin
  Result := FConexao.InTransaction;
end;

procedure TBancoDeDadosMySQL.ConfirmarTransacao;
begin
  if FTransacoes > 0 then
  begin
    Dec(FTransacoes);
    if FTransacoes = 0 then
      FConexao.Commit;
  end;
end;

procedure TBancoDeDadosMySQL.CancelarTransacao;
begin
  if FTransacoes > 0 then
  begin
    Dec(FTransacoes);
    if FTransacoes = 0 then
      FConexao.Rollback;
  end;
end;

end.
