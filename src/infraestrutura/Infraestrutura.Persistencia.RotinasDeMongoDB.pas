unit Infraestrutura.Persistencia.RotinasDeMongoDB;

interface

uses
  FireDAC.Comp.Client;

type
  //TRotinasDeMongoDB
  TRotinasDeMongoDB = class sealed
  public
    class function Conectar: TFDConnection;
  end;

implementation

uses
  System.SysUtils,
  FireDAC.Phys.MongoDB,
  FireDAC.Phys.MongoDBDef,
  Infraestrutura.Parametros,
  Infraestrutura.Parametros.Constantes;

{ TRotinasDeMongoDB }

class function TRotinasDeMongoDB.Conectar: TFDConnection;
var
  lParams: TFDPhysMongoConnectionDefParams;
begin
  Result := TFDConnection.Create(nil);
  try
    Result.DriverName := 'Mongo';
    lParams := TFDPhysMongoConnectionDefParams(Result.Params);
    lParams.Server := TParametros.Instancia.Ler([CPersistencia, CMongoDB, CServidor]);
    lParams.Port := StrToIntDef(TParametros.Instancia.Ler([CPersistencia, CMongoDB, CPorta]), 0);
    lParams.Database := TParametros.Instancia.Ler([CPersistencia, CMongoDB, CBancoDeDados]);
    lParams.UserName := TParametros.Instancia.Ler([CPersistencia, CMongoDB, CUsuario]);
    lParams.Password := TParametros.Instancia.Ler([CPersistencia, CMongoDB, CSenha]);
    Result.Open;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

end.
