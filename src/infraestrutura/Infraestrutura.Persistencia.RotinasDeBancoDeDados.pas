unit Infraestrutura.Persistencia.RotinasDeBancoDeDados;

interface

uses
  System.SysUtils;

type
  //TRotinasDeBancoDeDados
  TRotinasDeBancoDeDados = class sealed
  private class var
    FFormato: TFormatSettings;
  public
    class constructor Create;
    class function DataParaTexto(const AData: TDate): string;
    class function TextoParaData(const ATexto: string): TDate;
    class function DataHoraParaTexto(const AData: TDateTime): string; static;
    class function TextoParaDataHora(const ATexto: string): TDateTime; static;
  end;

implementation

{ TRotinasDeBancoDeDados }

class constructor TRotinasDeBancoDeDados.Create;
begin
  FFormato := TFormatSettings.Create;
  FFormato.DateSeparator := '-';
  FFormato.ShortDateFormat := 'YYYY-MM-DD';
end;

class function TRotinasDeBancoDeDados.DataParaTexto(const AData: TDate): string;
begin
  if (AData > 0) then
    Result := DateToStr(AData, FFormato)
  else
    Result := '';
end;

class function TRotinasDeBancoDeDados.TextoParaData(const ATexto: string): TDate;
begin
  if (ATexto <> '') then
    Result := StrToDate(ATexto, FFormato)
  else
    Result := 0;
end;

class function TRotinasDeBancoDeDados.DataHoraParaTexto(const AData: TDateTime): string;
begin
  if (AData > 0) then
    Result := DateTimeToStr(AData, FFormato)
  else
    Result := '';
end;

class function TRotinasDeBancoDeDados.TextoParaDataHora(const ATexto: string): TDateTime;
begin
  if (ATexto <> '') then
    Result := StrToDateTime(ATexto, FFormato)
  else
    Result := 0;
end;

end.
