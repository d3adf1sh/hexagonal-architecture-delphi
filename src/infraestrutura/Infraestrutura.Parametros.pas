unit Infraestrutura.Parametros;

interface

uses
  System.Generics.Collections;

type
  //TParametros
  TParametros = class sealed
  private class var
    FInstancia: TParametros;
  private
    FParametros: TDictionary<string, string>;
    function MontarNome(const APartes: array of string): string;
  public
    constructor Create;
    destructor Destroy; override;
    function Ler(const APartes: array of string): string; overload;
    function Ler(const ANome: string): string; overload;
    procedure Escrever(const APartes: array of string; const AValor: string); overload;
    procedure Escrever(const ANome, AValor: string); overload;
    procedure Carregar(const ANomeDoArquivo: string);
    class constructor Create;
    class destructor Destroy;
    class property Instancia: TParametros read FInstancia;
  end;

implementation

uses
  System.SysUtils,
  System.StrUtils,
  System.Classes,
  Infraestrutura.Parametros.Erros;

{ TParametros }

constructor TParametros.Create;
begin
  FParametros := TDictionary<string, string>.Create;
end;

destructor TParametros.Destroy;
begin
  FreeAndNil(FParametros);
  inherited;
end;

function TParametros.MontarNome(const APartes: array of string): string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to High(APartes) do
  begin
    if I > 0 then
      Result := Result + '.';
    Result := Result + APartes[I];
  end;
end;

function TParametros.Ler(const APartes: array of string): string;
begin
  Result := Ler(MontarNome(APartes));
end;

function TParametros.Ler(const ANome: string): string;
begin
  if not FParametros.TryGetValue(ANome, Result) then
    Result := '';
end;

procedure TParametros.Escrever(const APartes: array of string; const AValor: string);
begin
  Escrever(MontarNome(APartes), AValor);
end;

procedure TParametros.Escrever(const ANome, AValor: string);
begin
  FParametros.Add(ANome, AValor);
end;

procedure TParametros.Carregar(const ANomeDoArquivo: string);
var
  lArquivo: TStrings;
  sNome: string;
  I: Integer;
begin
  if not FileExists(ANomeDoArquivo) then
    raise EArquivoDeParametrosNaoEncontrado.CreateFmt('Arquivo de par�metros "%s" n�o encontrado.', [ANomeDoArquivo]);
  lArquivo := TStringList.Create;
  try
    lArquivo.LoadFromFile(ANomeDoArquivo);
    for I := 0 to Pred(lArquivo.Count) do
    begin
      sNome := Trim(lArquivo.Names[I]);
      if (sNome <> '') and not StartsStr('#', sNome) then
        Escrever(sNome, lArquivo.ValueFromIndex[I]);
    end;
  finally
    FreeAndNil(lArquivo);
  end;
end;

class constructor TParametros.Create;
begin
  FInstancia := TParametros.Create;
end;

class destructor TParametros.Destroy;
begin
  FreeAndNil(FInstancia);
end;

end.
