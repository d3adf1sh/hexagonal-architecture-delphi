unit Adaptadores.Entrada.REST.Controladores.Cliente.ControladorDeBuscaDeCliente;

interface

uses
  MVCFramework,
  MVCFramework.Commons,
  Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeBuscaDeCliente;

type
  //TControladorDeBuscaDeCliente
  [MVCPath('/cliente')]
  TControladorDeBuscaDeCliente = class(TMVCController)
  private
    FCasoDeUsoDeBuscaDeCliente: ICasoDeUsoDeBuscaDeCliente;
  public
    [MVCInject]
    constructor Create(const ACasoDeUsoDeBuscaDeCliente: ICasoDeUsoDeBuscaDeCliente); reintroduce;
    [MVCPath('/($ACPFJ)')]
    [MVCHTTPMethod([httpGET])]
    [MVCConsumes(TMVCMediaType.APPLICATION_JSON)]
    function Buscar(const ACPFJ: string): IMVCResponse;
  end;

implementation

uses
  System.SysUtils,
  Modelo.Entidades.Cliente,
  Modelo.Valores.CPFJ,
  Aplicacao.Portas.Entrada.Cliente.Erros,
  Adaptadores.Entrada.REST.Controladores.Cliente.ClienteWeb,
  Adaptadores.Entrada.REST.Conversores.ConversorDeCPFJ,
  Adaptadores.Entrada.REST.Resposta.ErroWeb,
  Adaptadores.Entrada.REST.Resposta.GeradorDeRespostaWeb;

{ TControladorDeBuscaDeCliente }

constructor TControladorDeBuscaDeCliente.Create(const ACasoDeUsoDeBuscaDeCliente: ICasoDeUsoDeBuscaDeCliente);
begin
  inherited Create;
  FCasoDeUsoDeBuscaDeCliente := ACasoDeUsoDeBuscaDeCliente;
end;

function TControladorDeBuscaDeCliente.Buscar(const ACPFJ: string): IMVCResponse;
var
  lCPFJ: TCPFJ;
  lCliente: TCliente;
begin
  try
    lCPFJ := TConversorDeCPFJ.De(ACPFJ);
    try
      lCliente := FCasoDeUsoDeBuscaDeCliente.Buscar(lCPFJ);
      try
        Result := TGeradorDeRespostaWeb.GerarResposta(HTTP_STATUS.OK, TClienteWeb.ConverterParaWeb(lCliente));
      finally
        FreeAndNil(lCliente);
      end;
    finally
      FreeAndNil(lCPFJ);
    end;
  except
    on E: EClienteNaoEncontrado do
      Result := TGeradorDeRespostaWeb.GerarResposta(HTTP_STATUS.NotFound, E.Message);
    on E: EErroWeb do
      Result := E.Resposta
    else
      raise;
  end;
end;

end.
