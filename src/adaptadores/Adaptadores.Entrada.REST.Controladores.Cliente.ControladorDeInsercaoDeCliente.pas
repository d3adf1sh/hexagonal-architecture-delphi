unit Adaptadores.Entrada.REST.Controladores.Cliente.ControladorDeInsercaoDeCliente;

interface

uses
  MVCFramework,
  MVCFramework.Commons,
  Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeInsercaoDeCliente,
  Adaptadores.Entrada.REST.Controladores.Cliente.ClienteWeb;

type
  //TControladorDeInsercaoDeCliente
  [MVCPath('/cliente')]
  TControladorDeInsercaoDeCliente = class(TMVCController)
  private
    FCasoDeUsoDeInsercaoDeCliente: ICasoDeUsoDeInsercaoDeCliente;
  public
    [MVCInject]
    constructor Create(const ACasoDeUsoDeInsercaoDeCliente: ICasoDeUsoDeInsercaoDeCliente); reintroduce;
    [MVCPath('')]
    [MVCHTTPMethod([httpPOST])]
    [MVCConsumes(TMVCMediaType.APPLICATION_JSON)]
    function Inserir(const [MVCFromBody] AClienteWeb: TClienteWeb): IMVCResponse;
  end;

implementation

uses
  System.SysUtils,
  Modelo.Entidades.Cliente,
  Aplicacao.Portas.Entrada.Cliente.Erros,
  Adaptadores.Entrada.REST.Resposta.ErroWeb,
  Adaptadores.Entrada.REST.Resposta.GeradorDeRespostaWeb;

{ TControladorDeInsercaoDeCliente }

constructor TControladorDeInsercaoDeCliente.Create(const ACasoDeUsoDeInsercaoDeCliente: ICasoDeUsoDeInsercaoDeCliente);
begin
  inherited Create;
  FCasoDeUsoDeInsercaoDeCliente := ACasoDeUsoDeInsercaoDeCliente;
end;

function TControladorDeInsercaoDeCliente.Inserir(const AClienteWeb: TClienteWeb): IMVCResponse;
var
  lCliente: TCliente;
  lClienteDeResposta: TCliente;
begin
  try
    lCliente := TClienteWeb.ConverterParaModelo(AClienteWeb);
    try
      lClienteDeResposta := FCasoDeUsoDeInsercaoDeCliente.Inserir(lCliente);
      try
        Result := TGeradorDeRespostaWeb.GerarResposta(HTTP_STATUS.Created,
          TClienteWeb.ConverterParaWeb(lClienteDeResposta));
      finally
        FreeAndNil(lClienteDeResposta);
      end;
    finally
      FreeAndNil(lCliente);
    end;
  except
    on E: EArgumentException do
      Result := TGeradorDeRespostaWeb.GerarResposta(HTTP_STATUS.BadRequest, E.Message);
    on E: EClienteJaCadastrado do
      Result := TGeradorDeRespostaWeb.GerarResposta(HTTP_STATUS.Conflict, E.Message);
    on E: EErroWeb do
      Result := E.Resposta
    else
      raise;
  end;
end;

end.
