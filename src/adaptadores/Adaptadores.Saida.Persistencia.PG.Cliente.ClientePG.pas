unit Adaptadores.Saida.Persistencia.PG.Cliente.ClientePG;

interface

uses
  System.Generics.Collections,
  Modelo.Entidades.Cliente;

type
  //Forward declarations
  TContatoPG = class;
  TTelefonePG = class;

  //TClientePG
  TClientePG = class sealed
  private
    FIDCliente: Integer;
    FNomeFantasia: string;
    FRazaoSocial: string;
    FCPFJ: string;
    FContatos: TObjectList<TContatoPG>;
    FDataDeInsercao: TDateTime;
    FDataDeAlteracao: TDateTime;
  public
    destructor Destroy; override;
    property IDCliente: Integer read FIDCliente write FIDCliente;
    property NomeFantasia: string read FNomeFantasia write FNomeFantasia;
    property RazaoSocial: string read FRazaoSocial write FRazaoSocial;
    property CPFJ: string read FCPFJ write FCPFJ;
    property Contatos: TObjectList<TContatoPG> read FContatos write FContatos;
    property DataDeInsercao: TDateTime read FDataDeInsercao write FDataDeInsercao;
    property DataDeAlteracao: TDateTime read FDataDeAlteracao write FDataDeAlteracao;
    class function ConverterParaPG(const AClientes: TObjectList<TCliente>): TObjectList<TClientePG>; overload;
    class function ConverterParaPG(const ACliente: TCliente): TClientePG; overload;
    class function ConverterParaModelo(const AClientesPG: TObjectList<TClientePG>): TObjectList<TCliente>; overload;
    class function ConverterParaModelo(const AClientePG: TClientePG): TCliente; overload;
  end;

  //TContatoPG
  TContatoPG = class sealed
  private
    FIDContato: Integer;
    FCliente: TClientePG;
    FNome: string;
    FTelefones: TObjectList<TTelefonePG>;
  public
    destructor Destroy; override;
    property IDContato: Integer read FIDContato write FIDContato;
    property Cliente: TClientePG read FCliente write FCliente;
    property Nome: string read FNome write FNome;
    property Telefones: TObjectList<TTelefonePG> read FTelefones write FTelefones;
    class function ConverterParaPG(const ACliente: TClientePG;
      const AContatos: TObjectList<TContato>): TObjectList<TContatoPG>; overload;
    class function ConverterParaPG(const ACliente: TClientePG; const AContato: TContato): TContatoPG; overload;
    class function ConverterParaModelo(const AContatosPG: TObjectList<TContatoPG>): TObjectList<TContato>; overload;
    class function ConverterParaModelo(const AContatoPG: TContatoPG): TContato; overload;
  end;

  //TTelefonePG
  TTelefonePG = class sealed
  private
    FIDTelefone: Integer;
    FContato: TContatoPG;
    FDescricao: string;
    FNumero: string;
  public
    property IDTelefone: Integer read FIDTelefone write FIDTelefone;
    property Contato: TContatoPG read FContato write FContato;
    property Descricao: string read FDescricao write FDescricao;
    property Numero: string read FNumero write FNumero;
    class function ConverterParaPG(const AContato: TContatoPG;
      const ATelefones: TObjectList<TTelefone>): TObjectList<TTelefonePG>; overload;
    class function ConverterParaPG(const AContato: TContatoPG; const ATelefone: TTelefone): TTelefonePG; overload;
    class function ConverterParaModelo(const ATelefonesPG: TObjectList<TTelefonePG>): TObjectList<TTelefone>; overload;
    class function ConverterParaModelo(const ATelefonePG: TTelefonePG): TTelefone; overload;
  end;

implementation

uses
  System.SysUtils,
  Modelo.Valores.CPFJ,
  Modelo.Valores.Numero;

{ TClientePG }

destructor TClientePG.Destroy;
begin
  if Assigned(FContatos) then
    FreeAndNil(FContatos);
  inherited;
end;

class function TClientePG.ConverterParaPG(const AClientes: TObjectList<TCliente>): TObjectList<TClientePG>;
var
  I: Integer;
begin
  Result := TObjectList<TClientePG>.Create(True);
  try
    for I := 0 to Pred(AClientes.Count) do
      Result.Add(ConverterParaPG(AClientes[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TClientePG.ConverterParaPG(const ACliente: TCliente): TClientePG;
begin
  Result := TClientePG.Create;
  try
    Result.NomeFantasia := ACliente.NomeFantasia;
    Result.RazaoSocial := ACliente.RazaoSocial;
    if Assigned(ACliente.CPFJ) then
      Result.CPFJ := ACliente.CPFJ.Valor
    else
      Result.CPFJ := '';
    if Assigned(ACliente.Contatos) then
      Result.Contatos := TContatoPG.ConverterParaPG(Result, ACliente.Contatos)
    else
      Result.Contatos := nil;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TClientePG.ConverterParaModelo(const AClientesPG: TObjectList<TClientePG>): TObjectList<TCliente>;
var
  I: Integer;
begin
  Result := TObjectList<TCliente>.Create(True);
  try
    for I := 0 to Pred(AClientesPG.Count) do
      Result.Add(ConverterParaModelo(AClientesPG[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TClientePG.ConverterParaModelo(const AClientePG: TClientePG): TCliente;
var
  lCPFJ: TCPFJ;
  lContatos: TObjectList<TContato>;
begin
  lCPFJ := nil;
  lContatos := nil;
  try
    if AClientePG.CPFJ <> '' then
      lCPFJ := TCPFJ.Create(AClientePG.CPFJ);
    if Assigned(AClientePG.Contatos) then
      lContatos := TContatoPG.ConverterParaModelo(AClientePG.Contatos);
    Result := TCliente.Create(AClientePG.NomeFantasia, AClientePG.RazaoSocial, lCPFJ, lContatos);
  except
    if Assigned(lCPFJ) then
      FreeAndNil(lCPFJ);
    if Assigned(lContatos) then
      FreeAndNil(lContatos);
    raise;
  end;
end;

{ TContatoPG }

destructor TContatoPG.Destroy;
begin
  if Assigned(FTelefones) then
    FreeAndNil(FTelefones);
  inherited;
end;

class function TContatoPG.ConverterParaPG(const ACliente: TClientePG;
  const AContatos: TObjectList<TContato>): TObjectList<TContatoPG>;
var
  I: Integer;
begin
  Result := TObjectList<TContatoPG>.Create(True);
  try
    for I := 0 to Pred(AContatos.Count) do
      Result.Add(ConverterParaPG(ACliente, AContatos[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TContatoPG.ConverterParaPG(const ACliente: TClientePG; const AContato: TContato): TContatoPG;
begin
  Result := TContatoPG.Create;
  try
    Result.Cliente := ACliente;
    Result.Nome := AContato.Nome;
    if Assigned(AContato.Telefones) then
      Result.Telefones := TTelefonePG.ConverterParaPG(Result, AContato.Telefones)
    else
      Result.Telefones := nil;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TContatoPG.ConverterParaModelo(const AContatosPG: TObjectList<TContatoPG>): TObjectList<TContato>;
var
  I: Integer;
begin
  Result := TObjectList<TContato>.Create(True);
  try
    for I := 0 to Pred(AContatosPG.Count) do
      Result.Add(ConverterParaModelo(AContatosPG[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TContatoPG.ConverterParaModelo(const AContatoPG: TContatoPG): TContato;
var
  lTelefones: TObjectList<TTelefone>;
begin
  lTelefones := nil;
  try
    if Assigned(AContatoPG.Telefones) then
      lTelefones := TTelefonePG.ConverterParaModelo(AContatoPG.Telefones);
    Result := TContato.Create(AContatoPG.Nome, lTelefones);
  except
    if Assigned(lTelefones) then
      FreeAndNil(lTelefones);
    raise;
  end;
end;

{ TTelefonePG }

class function TTelefonePG.ConverterParaPG(const AContato: TContatoPG;
  const ATelefones: TObjectList<TTelefone>): TObjectList<TTelefonePG>;
var
  I: Integer;
begin
  Result := TObjectList<TTelefonePG>.Create(True);
  try
    for I := 0 to Pred(ATelefones.Count) do
      Result.Add(ConverterParaPG(AContato, ATelefones[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TTelefonePG.ConverterParaPG(const AContato: TContatoPG; const ATelefone: TTelefone): TTelefonePG;
begin
  Result := TTelefonePG.Create;
  try
    Result.Contato := AContato;
    Result.Descricao := ATelefone.Descricao;
    if Assigned(ATelefone.Numero) then
      Result.Numero := ATelefone.Numero.Valor
    else
      Result.Numero := '';
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TTelefonePG.ConverterParaModelo(const ATelefonesPG: TObjectList<TTelefonePG>): TObjectList<TTelefone>;
var
  I: Integer;
begin
  Result := TObjectList<TTelefone>.Create(True);
  try
    for I := 0 to Pred(ATelefonesPG.Count) do
      Result.Add(ConverterParaModelo(ATelefonesPG[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TTelefonePG.ConverterParaModelo(const ATelefonePG: TTelefonePG): TTelefone;
var
  lNumero: TNumero;
begin
  lNumero := nil;
  try
    if ATelefonePG.Numero <> '' then
      lNumero := TNumero.Create(ATelefonePG.Numero);
    Result := TTelefone.Create(ATelefonePG.Descricao, lNumero);
  except
    if Assigned(lNumero) then
      FreeAndNil(lNumero);
    raise;
  end;
end;

end.
