unit Adaptadores.Entrada.REST.Conversores.ConversorDeNumero;

interface

uses
  Modelo.Valores.Numero;

type
  //TConversorDeNumero
  TConversorDeNumero = class sealed
  public
    class function De(const ANumero: string): TNumero;
  end;

implementation

uses
  System.SysUtils,
  MVCFramework.Commons,
  Adaptadores.Entrada.REST.Resposta.GeradorDeRespostaWeb;

{ TConversorDeNumero }

class function TConversorDeNumero.De(const ANumero: string): TNumero;
begin
  try
    Result := TNumero.De(ANumero);
  except
    on E: EArgumentException do
      raise TGeradorDeRespostaWeb.GerarErro(HTTP_STATUS.BadRequest, 'N�mero inv�lido.');
    else
      raise;
  end;
end;

end.
