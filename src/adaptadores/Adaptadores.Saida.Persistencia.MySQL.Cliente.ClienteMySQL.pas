unit Adaptadores.Saida.Persistencia.MySQL.Cliente.ClienteMySQL;

interface

uses
  System.Generics.Collections,
  Modelo.Entidades.Cliente;

type
  //Forward declarations
  TContatoMySQL = class;
  TTelefoneMySQL = class;

  //TClienteMySQL
  TClienteMySQL = class sealed
  private
    FIDCliente: Integer;
    FNomeFantasia: string;
    FRazaoSocial: string;
    FCPFJ: string;
    FContatos: TObjectList<TContatoMySQL>;
    FDataDeInsercao: TDateTime;
    FDataDeAlteracao: TDateTime;
  public
    destructor Destroy; override;
    property IDCliente: Integer read FIDCliente write FIDCliente;
    property NomeFantasia: string read FNomeFantasia write FNomeFantasia;
    property RazaoSocial: string read FRazaoSocial write FRazaoSocial;
    property CPFJ: string read FCPFJ write FCPFJ;
    property Contatos: TObjectList<TContatoMySQL> read FContatos write FContatos;
    property DataDeInsercao: TDateTime read FDataDeInsercao write FDataDeInsercao;
    property DataDeAlteracao: TDateTime read FDataDeAlteracao write FDataDeAlteracao;
    class function ConverterParaMySQL(const AClientes: TObjectList<TCliente>): TObjectList<TClienteMySQL>; overload;
    class function ConverterParaMySQL(const ACliente: TCliente): TClienteMySQL; overload;
    class function ConverterParaModelo(const AClientesMySQL: TObjectList<TClienteMySQL>): TObjectList<TCliente>; overload;
    class function ConverterParaModelo(const AClienteMySQL: TClienteMySQL): TCliente; overload;
  end;

  //TContatoMySQL
  TContatoMySQL = class sealed
  private
    FIDContato: Integer;
    FCliente: TClienteMySQL;
    FNome: string;
    FTelefones: TObjectList<TTelefoneMySQL>;
  public
    destructor Destroy; override;
    property IDContato: Integer read FIDContato write FIDContato;
    property Cliente: TClienteMySQL read FCliente write FCliente;
    property Nome: string read FNome write FNome;
    property Telefones: TObjectList<TTelefoneMySQL> read FTelefones write FTelefones;
    class function ConverterParaMySQL(const ACliente: TClienteMySQL;
      const AContatos: TObjectList<TContato>): TObjectList<TContatoMySQL>; overload;
    class function ConverterParaMySQL(const ACliente: TClienteMySQL; const AContato: TContato): TContatoMySQL; overload;
    class function ConverterParaModelo(const AContatosMySQL: TObjectList<TContatoMySQL>): TObjectList<TContato>; overload;
    class function ConverterParaModelo(const AContatoMySQL: TContatoMySQL): TContato; overload;
  end;

  //TTelefoneMySQL
  TTelefoneMySQL = class sealed
  private
    FIDTelefone: Integer;
    FContato: TContatoMySQL;
    FDescricao: string;
    FNumero: string;
  public
    property IDTelefone: Integer read FIDTelefone write FIDTelefone;
    property Contato: TContatoMySQL read FContato write FContato;
    property Descricao: string read FDescricao write FDescricao;
    property Numero: string read FNumero write FNumero;
    class function ConverterParaMySQL(const AContato: TContatoMySQL;
      const ATelefones: TObjectList<TTelefone>): TObjectList<TTelefoneMySQL>; overload;
    class function ConverterParaMySQL(const AContato: TContatoMySQL; const ATelefone: TTelefone): TTelefoneMySQL; overload;
    class function ConverterParaModelo(const ATelefonesMySQL: TObjectList<TTelefoneMySQL>): TObjectList<TTelefone>; overload;
    class function ConverterParaModelo(const ATelefoneMySQL: TTelefoneMySQL): TTelefone; overload;
  end;

implementation

uses
  System.SysUtils,
  Modelo.Valores.CPFJ,
  Modelo.Valores.Numero;

{ TClienteMySQL }

destructor TClienteMySQL.Destroy;
begin
  if Assigned(FContatos) then
    FreeAndNil(FContatos);
  inherited;
end;

class function TClienteMySQL.ConverterParaMySQL(const AClientes: TObjectList<TCliente>): TObjectList<TClienteMySQL>;
var
  I: Integer;
begin
  Result := TObjectList<TClienteMySQL>.Create(True);
  try
    for I := 0 to Pred(AClientes.Count) do
      Result.Add(ConverterParaMySQL(AClientes[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TClienteMySQL.ConverterParaMySQL(const ACliente: TCliente): TClienteMySQL;
begin
  Result := TClienteMySQL.Create;
  try
    Result.NomeFantasia := ACliente.NomeFantasia;
    Result.RazaoSocial := ACliente.RazaoSocial;
    if Assigned(ACliente.CPFJ) then
      Result.CPFJ := ACliente.CPFJ.Valor
    else
      Result.CPFJ := '';
    if Assigned(ACliente.Contatos) then
      Result.Contatos := TContatoMySQL.ConverterParaMySQL(Result, ACliente.Contatos)
    else
      Result.Contatos := nil;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TClienteMySQL.ConverterParaModelo(
  const AClientesMySQL: TObjectList<TClienteMySQL>): TObjectList<TCliente>;
var
  I: Integer;
begin
  Result := TObjectList<TCliente>.Create(True);
  try
    for I := 0 to Pred(AClientesMySQL.Count) do
      Result.Add(ConverterParaModelo(AClientesMySQL[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TClienteMySQL.ConverterParaModelo(const AClienteMySQL: TClienteMySQL): TCliente;
var
  lCPFJ: TCPFJ;
  lContatos: TObjectList<TContato>;
begin
  lCPFJ := nil;
  lContatos := nil;
  try
    if AClienteMySQL.CPFJ <> '' then
      lCPFJ := TCPFJ.Create(AClienteMySQL.CPFJ);
    if Assigned(AClienteMySQL.Contatos) then
      lContatos := TContatoMySQL.ConverterParaModelo(AClienteMySQL.Contatos);
    Result := TCliente.Create(AClienteMySQL.NomeFantasia, AClienteMySQL.RazaoSocial, lCPFJ, lContatos);
  except
    if Assigned(lCPFJ) then
      FreeAndNil(lCPFJ);
    if Assigned(lContatos) then
      FreeAndNil(lContatos);
    raise;
  end;
end;

{ TContatoMySQL }

destructor TContatoMySQL.Destroy;
begin
  if Assigned(FTelefones) then
    FreeAndNil(FTelefones);
  inherited;
end;

class function TContatoMySQL.ConverterParaMySQL(const ACliente: TClienteMySQL;
  const AContatos: TObjectList<TContato>): TObjectList<TContatoMySQL>;
var
  I: Integer;
begin
  Result := TObjectList<TContatoMySQL>.Create(True);
  try
    for I := 0 to Pred(AContatos.Count) do
      Result.Add(ConverterParaMySQL(ACliente, AContatos[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TContatoMySQL.ConverterParaMySQL(const ACliente: TClienteMySQL; const AContato: TContato): TContatoMySQL;
begin
  Result := TContatoMySQL.Create;
  try
    Result.Cliente := ACliente;
    Result.Nome := AContato.Nome;
    if Assigned(AContato.Telefones) then
      Result.Telefones := TTelefoneMySQL.ConverterParaMySQL(Result, AContato.Telefones)
    else
      Result.Telefones := nil;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TContatoMySQL.ConverterParaModelo(
  const AContatosMySQL: TObjectList<TContatoMySQL>): TObjectList<TContato>;
var
  I: Integer;
begin
  Result := TObjectList<TContato>.Create(True);
  try
    for I := 0 to Pred(AContatosMySQL.Count) do
      Result.Add(ConverterParaModelo(AContatosMySQL[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TContatoMySQL.ConverterParaModelo(const AContatoMySQL: TContatoMySQL): TContato;
var
  lTelefones: TObjectList<TTelefone>;
begin
  lTelefones := nil;
  try
    if Assigned(AContatoMySQL.Telefones) then
      lTelefones := TTelefoneMySQL.ConverterParaModelo(AContatoMySQL.Telefones);
    Result := TContato.Create(AContatoMySQL.Nome, lTelefones);
  except
    if Assigned(lTelefones) then
      FreeAndNil(lTelefones);
    raise;
  end;
end;

{ TTelefoneMySQL }

class function TTelefoneMySQL.ConverterParaMySQL(const AContato: TContatoMySQL;
  const ATelefones: TObjectList<TTelefone>): TObjectList<TTelefoneMySQL>;
var
  I: Integer;
begin
  Result := TObjectList<TTelefoneMySQL>.Create(True);
  try
    for I := 0 to Pred(ATelefones.Count) do
      Result.Add(ConverterParaMySQL(AContato, ATelefones[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TTelefoneMySQL.ConverterParaMySQL(const AContato: TContatoMySQL;
  const ATelefone: TTelefone): TTelefoneMySQL;
begin
  Result := TTelefoneMySQL.Create;
  try
    Result.Contato := AContato;
    Result.Descricao := ATelefone.Descricao;
    if Assigned(ATelefone.Numero) then
      Result.Numero := ATelefone.Numero.Valor
    else
      Result.Numero := '';
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TTelefoneMySQL.ConverterParaModelo(
  const ATelefonesMySQL: TObjectList<TTelefoneMySQL>): TObjectList<TTelefone>;
var
  I: Integer;
begin
  Result := TObjectList<TTelefone>.Create(True);
  try
    for I := 0 to Pred(ATelefonesMySQL.Count) do
      Result.Add(ConverterParaModelo(ATelefonesMySQL[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TTelefoneMySQL.ConverterParaModelo(const ATelefoneMySQL: TTelefoneMySQL): TTelefone;
var
  lNumero: TNumero;
begin
  lNumero := nil;
  try
    if ATelefoneMySQL.Numero <> '' then
      lNumero := TNumero.Create(ATelefoneMySQL.Numero);
    Result := TTelefone.Create(ATelefoneMySQL.Descricao, lNumero);
  except
    if Assigned(lNumero) then
      FreeAndNil(lNumero);
    raise;
  end;
end;

end.
