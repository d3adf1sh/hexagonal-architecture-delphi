unit Adaptadores.Saida.Persistencia.MongoDB.Cliente.RepositorioDeClientesMongoDB;

interface

uses
  System.SysUtils,
  System.Generics.Collections,
  System.JSON,
  FireDAC.Phys.MongoDBWrapper,
  Modelo.Entidades.Cliente,
  Modelo.Valores.CPFJ,
  Aplicacao.Portas.Saida.RepositorioDeClientes;

type
  //TRepositorioDeClientesMongoDB
  TRepositorioDeClientesMongoDB = class sealed(TInterfacedObject, IRepositorioDeClientes)
  private
    procedure InserirCliente(const AJSONDeCliente: TJSONObject; const AColecao: TMongoCollection);
    procedure AlterarCliente(const AJSONDeCliente: TJSONObject; const AColecao: TMongoCollection);
    procedure ExcluirCliente(const AJSONDeCliente: TJSONObject; const AColecao: TMongoCollection);
    function CarregarClientes(const ANomeFantasia: string; const AColecao: TMongoCollection): TJSONArray;
    function CarregarCliente(const ACPFJ: string; const AColecao: TMongoCollection): TJSONObject;
    function LerClientes(const ACursor: IMongoCursor): TJSONArray;
    function LerCliente(const ACursor: IMongoCursor): TJSONObject;
  public
    function Inserir(const ACliente: TCliente): TCliente;
    function Alterar(const ACliente: TCliente): TCliente;
    function Excluir(const ACPFJ: TCPFJ): TCliente;
    function Buscar(const ACPFJ: TCPFJ): TCliente;
    function Consultar(const ANomeFantasia: string): TObjectList<TCliente>;
  end;

implementation

uses
  System.JSON.Types,
  FireDAC.Comp.Client,
  Infraestrutura.Persistencia.RotinasDeMongoDB,
  Adaptadores.Saida.Persistencia.MongoDB.Cliente.ClienteMongoDB;

{ TRepositorioDeClientesMongoDB }

procedure TRepositorioDeClientesMongoDB.InserirCliente(const AJSONDeCliente: TJSONObject;
  const AColecao: TMongoCollection);
begin
  AColecao.Insert
    .Values(AJSONDeCliente.ToJSON)
      .Add(TClienteMongoDB.DataDeInsercao, Now)
      .&End
    .Exec;
end;

procedure TRepositorioDeClientesMongoDB.AlterarCliente(const AJSONDeCliente: TJSONObject;
  const AColecao: TMongoCollection);
var
  lCPFJ: TJSONValue;
  sCPFJ: string;
begin
  lCPFJ := AJSONDeCliente.GetValue(TClienteMongoDB.CPFJ);
  if Assigned(lCPFJ) then
    sCPFJ := lCPFJ.Value
  else
    sCPFJ := '';
  AColecao.Update
    .Match
      .Add(TClienteMongoDB.CPFJ, sCPFJ)
      .&End
    .Modify
      .&Set(AJSONDeCliente.ToJSON)
        .Field(TClienteMongoDB.DataDeAlteracao, Now)
        .&End
      .&End
    .Exec;
end;

procedure TRepositorioDeClientesMongoDB.ExcluirCliente(const AJSONDeCliente: TJSONObject;
  const AColecao: TMongoCollection);
var
  lCPFJ: TJSONValue;
  sCPFJ: string;
begin
  lCPFJ := AJSONDeCliente.GetValue(TClienteMongoDB.CPFJ);
  if Assigned(lCPFJ) then
    sCPFJ := lCPFJ.Value
  else
    sCPFJ := '';
  AColecao.Remove
    .Match
      .Add(TClienteMongoDB.CPFJ, sCPFJ)
      .&End
    .Exec;
end;

function TRepositorioDeClientesMongoDB.CarregarClientes(const ANomeFantasia: string;
  const AColecao: TMongoCollection): TJSONArray;
var
  lConsulta: TMongoQuery;
  lCursor: IMongoCursor;
begin
  lConsulta := AColecao.Find;
  if ANomeFantasia <> '' then
    lConsulta.Match
      .Add(TClienteMongoDB.NomeFantasia, TJSONRegEx.Create(ANomeFantasia, ''));
  lCursor := lConsulta.Open;
  Result := LerClientes(lCursor);
end;

function TRepositorioDeClientesMongoDB.CarregarCliente(const ACPFJ: string;
  const AColecao: TMongoCollection): TJSONObject;
var
  lCursor: IMongoCursor;
begin
  lCursor := AColecao.Find
    .Match
      .Add(TClienteMongoDB.CPFJ, ACPFJ)
      .&End
    .Open;
  if lCursor.Next then
    Result := LerCliente(lCursor)
  else
    Result := nil;
end;

function TRepositorioDeClientesMongoDB.LerClientes(const ACursor: IMongoCursor): TJSONArray;
begin
  Result := TJSONArray.Create;
  try
    while ACursor.Next do
      Result.Add(LerCliente(ACursor));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TRepositorioDeClientesMongoDB.LerCliente(const ACursor: IMongoCursor): TJSONObject;
begin
  Result := TJSONObject.Create;
  try
    Result.Parse(TEncoding.UTF8.GetBytes(ACursor.Doc.AsJSON), 0);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TRepositorioDeClientesMongoDB.Inserir(const ACliente: TCliente): TCliente;
var
  lConexao: TFDConnection;
  lBancoDeDados: TMongoDatabase;
  lColecao: TMongoCollection;
  lJSONDeCliente: TJSONObject;
begin
  lConexao := TRotinasDeMongoDB.Conectar;
  try
    lBancoDeDados := TMongoConnection(lConexao.CliObj).GetDatabase(lConexao.Params.Database);
    try
      lColecao := lBancoDeDados.GetCollection(TClienteMongoDB.Cliente);
      try
        lJSONDeCliente := TClienteMongoDB.ConverterParaJSON(ACliente);
        try
          InserirCliente(lJSONDeCliente, lColecao);
          Result := TClienteMongoDB.ConverterParaModelo(lJSONDeCliente);
        finally
          FreeAndNil(lJSONDeCliente);
        end;
      finally
        FreeAndNil(lColecao);
      end;
    finally
      FreeAndNil(lBancoDeDados);
    end;
  finally
    FreeAndNil(lConexao);
  end;
end;

function TRepositorioDeClientesMongoDB.Alterar(const ACliente: TCliente): TCliente;
var
  lConexao: TFDConnection;
  lBancoDeDados: TMongoDatabase;
  lColecao: TMongoCollection;
  lJSONDeClienteExistente: TJSONObject;
  lJSONDeCliente: TJSONObject;
begin
  lConexao := TRotinasDeMongoDB.Conectar;
  try
    lBancoDeDados := TMongoConnection(lConexao.CliObj).GetDatabase(lConexao.Params.Database);
    try
      lColecao := lBancoDeDados.GetCollection(TClienteMongoDB.Cliente);
      try
        lJSONDeClienteExistente := CarregarCliente(ACliente.CPFJ.Valor, lColecao);
        if Assigned(lJSONDeClienteExistente) then
          try
            lJSONDeCliente := TClienteMongoDB.ConverterParaJSON(ACliente);
            try
              AlterarCliente(lJSONDeCliente, lColecao);
              Result := TClienteMongoDB.ConverterParaModelo(lJSONDeCliente);
            finally
              FreeAndNil(lJSONDeCliente);
            end;
          finally
            FreeAndNil(lJSONDeClienteExistente);
          end
        else
          Result := nil;
      finally
        FreeAndNil(lColecao);
      end;
    finally
      FreeAndNil(lBancoDeDados);
    end;
  finally
    FreeAndNil(lConexao);
  end;
end;

function TRepositorioDeClientesMongoDB.Excluir(const ACPFJ: TCPFJ): TCliente;
var
  lConexao: TFDConnection;
  lBancoDeDados: TMongoDatabase;
  lColecao: TMongoCollection;
  lJSONDeCliente: TJSONObject;
begin
  lConexao := TRotinasDeMongoDB.Conectar;
  try
    lBancoDeDados := TMongoConnection(lConexao.CliObj).GetDatabase(lConexao.Params.Database);
    try
      lColecao := lBancoDeDados.GetCollection(TClienteMongoDB.Cliente);
      try
        lJSONDeCliente := CarregarCliente(ACPFJ.Valor, lColecao);
        if Assigned(lJSONDeCliente) then
          try
            ExcluirCliente(lJSONDeCliente, lColecao);
            Result := TClienteMongoDB.ConverterParaModelo(lJSONDeCliente);
          finally
            FreeAndNil(lJSONDeCliente);
          end
        else
          Result := nil;
      finally
        FreeAndNil(lColecao);
      end;
    finally
      FreeAndNil(lBancoDeDados);
    end;
  finally
    FreeAndNil(lConexao);
  end;
end;

function TRepositorioDeClientesMongoDB.Buscar(const ACPFJ: TCPFJ): TCliente;
var
  lConexao: TFDConnection;
  lBancoDeDados: TMongoDatabase;
  lColecao: TMongoCollection;
  lJSONDeCliente: TJSONObject;
begin
  lConexao := TRotinasDeMongoDB.Conectar;
  try
    lBancoDeDados := TMongoConnection(lConexao.CliObj).GetDatabase(lConexao.Params.Database);
    try
      lColecao := lBancoDeDados.GetCollection(TClienteMongoDB.Cliente);
      try
        lJSONDeCliente := CarregarCliente(ACPFJ.Valor, lColecao);
        if Assigned(lJSONDeCliente) then
          try
            Result := TClienteMongoDB.ConverterParaModelo(lJSONDeCliente);
          finally
            FreeAndNil(lJSONDeCliente);
          end
        else
          Result := nil;
      finally
        FreeAndNil(lColecao);
      end;
    finally
      FreeAndNil(lBancoDeDados);
    end;
  finally
    FreeAndNil(lConexao);
  end;
end;

function TRepositorioDeClientesMongoDB.Consultar(const ANomeFantasia: string): TObjectList<TCliente>;
var
  lConexao: TFDConnection;
  lBancoDeDados: TMongoDatabase;
  lColecao: TMongoCollection;
  lJSONDeClientes: TJSONArray;
begin
  lConexao := TRotinasDeMongoDB.Conectar;
  try
    lBancoDeDados := TMongoConnection(lConexao.CliObj).GetDatabase(lConexao.Params.Database);
    try
      lColecao := lBancoDeDados.GetCollection(TClienteMongoDB.Cliente);
      try
        lJSONDeClientes := CarregarClientes(ANomeFantasia, lColecao);
        try
          Result := TClienteMongoDB.ConverterParaModelo(lJSONDeClientes);
        finally
          FreeAndNil(lJSONDeClientes);
        end;
      finally
        FreeAndNil(lColecao);
      end;
    finally
      FreeAndNil(lBancoDeDados);
    end;
  finally
    FreeAndNil(lConexao);
  end;
end;

end.

