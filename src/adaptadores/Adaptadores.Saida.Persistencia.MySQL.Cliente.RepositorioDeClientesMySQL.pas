unit Adaptadores.Saida.Persistencia.MySQL.Cliente.RepositorioDeClientesMySQL;

interface

uses
  System.SysUtils,
  System.Generics.Collections,
  Data.DB,
  Infraestrutura.Persistencia.BancoDeDados,
  Infraestrutura.Persistencia.FabricaDeBancoDeDados,
  Modelo.Entidades.Cliente,
  Modelo.Valores.CPFJ,
  Aplicacao.Portas.Saida.RepositorioDeClientes,
  Adaptadores.Saida.Persistencia.MySQL.Cliente.ClienteMySQL;

type
  //Forward declarations
  TRepositorioDeContatosMySQL = class;
  TRepositorioDeTelefonesMySQL = class;

  //TRepositorioDeClientesMySQL
  TRepositorioDeClientesMySQL = class sealed(TInterfacedObject, IRepositorioDeClientes)
  private const
    FCliente = 'Cliente';
    FIDCliente = 'idCliente';
    FNomeFantasia  = 'nomeFantasia';
    FRazaoSocial = 'razaoSocial';
    FCPFJ = 'cpfj';
    FDataDeInsercao = 'dataDeInsercao';
    FDataDeAlteracao = 'dataDeAlteracao';
  private
    FFabricaDeBancoDeDados: IFabricaDeBancoDeDados;
    FRepositorioDeContatos: TRepositorioDeContatosMySQL;
    procedure InserirCliente(const AClienteMySQL: TClienteMySQL; const ABancoDeDados: IBancoDeDados);
    procedure AlterarCliente(const AClienteMySQL: TClienteMySQL; const ABancoDeDados: IBancoDeDados);
    procedure ExcluirCliente(const AClienteMySQL: TClienteMySQL; const ABancoDeDados: IBancoDedados);
    function CarregarClientes(const ANomeFantasia: string;
      const ABancoDeDados: IBancoDeDados): TObjectList<TClienteMySQL>;
    function CarregarCliente(const ACPFJ: string; const ABancoDeDados: IBancoDeDados): TClienteMySQL;
    function LerClientes(const ADataSet: TDataSet; const ABancoDeDados: IBancoDeDados): TObjectList<TClienteMySQL>;
    function LerCliente(const ADataSet: TDataSet; const ABancoDeDados: IBancoDeDados): TClienteMySQL;
  public
    constructor Create(const AFabricaDeBancoDeDados: IFabricaDeBancoDeDados);
    destructor Destroy; override;
    function Inserir(const ACliente: TCliente): TCliente;
    function Alterar(const ACliente: TCliente): TCliente;
    function Excluir(const ACPFJ: TCPFJ): TCliente;
    function Buscar(const ACPFJ: TCPFJ): TCliente;
    function Consultar(const ANomeFantasia: string): TObjectList<TCliente>;
  end;

  //TRepositorioDeContatosMySQL
  TRepositorioDeContatosMySQL = class sealed
  private const
    FContato = 'Contato';
    FIDContato = 'idContato';
    FIDCliente = 'idCliente';
    FNome = 'nome';
  private
    FRepositorioDeTelefones: TRepositorioDeTelefonesMySQL;
    procedure InserirContatos(const AContatosMySQL: TObjectList<TContatoMySQL>; const ABancoDeDados: IBancoDeDados);
    procedure InserirContato(const AContatoMySQL: TContatoMySQL; const ABancoDeDados: IBancoDeDados);
    procedure AlterarContatos(const AContatosMySQL: TObjectList<TContatoMySQL>; const ABancoDeDados: IBancoDeDados);
    procedure AlterarContato(const AContatoMySQL: TContatoMySQL; const ABancoDeDados: IBancoDeDados);
    procedure ExcluirContatos(const AContatosMySQL: TObjectList<TContatoMySQL>; const ABancoDeDados: IBancoDedados);
    procedure ExcluirContato(const AContatoMySQL: TContatoMySQL; const ABancoDeDados: IBancoDedados);
    function CarregarContatos(const AIDCliente: Integer;
      const ABancoDeDados: IBancoDeDados): TObjectList<TContatoMySQL>;
    function LerContatos(const ADataSet: TDataSet; const ABancoDeDados: IBancoDeDados): TObjectList<TContatoMySQL>;
    function LerContato(const ADataSet: TDataSet; const ABancoDeDados: IBancoDeDados): TContatoMySQL;
  public
    constructor Create;
    destructor Destroy; override;
  end;

  //TRepositorioDeTelefonesMySQL
  TRepositorioDeTelefonesMySQL = class sealed
  private const
    FTelefone = 'Telefone';
    FIDTelefone = 'IDTelefone';
    FIDContato = 'IDContato';
    FDescricao = 'Descricao';
    FNumero = 'Numero';
  private
    procedure InserirTelefones(const ATelefonesMySQL: TObjectList<TTelefoneMySQL>; const ABancoDeDados: IBancoDeDados);
    procedure InserirTelefone(const ATelefoneMySQL: TTelefoneMySQL; const ABancoDeDados: IBancoDeDados);
    procedure AlterarTelefones(const ATelefonesMySQL: TObjectList<TTelefoneMySQL>; const ABancoDeDados: IBancoDeDados);
    procedure AlterarTelefone(const ATelefoneMySQL: TTelefoneMySQL; const ABancoDeDados: IBancoDeDados);
    procedure ExcluirTelefones(const ATelefonesMySQL: TObjectList<TTelefoneMySQL>; const ABancoDeDados: IBancoDedados);
    procedure ExcluirTelefone(const ATelefoneMySQL: TTelefoneMySQL; const ABancoDeDados: IBancoDedados);
    function CarregarTelefones(const AIDContato: Integer;
      const ABancoDeDados: IBancoDeDados): TObjectList<TTelefoneMySQL>;
    function LerTelefones(const ADataSet: TDataSet): TObjectList<TTelefoneMySQL>;
    function LerTelefone(const ADataSet: TDataSet): TTelefoneMySQL;
  end;

implementation

uses
  System.Classes,
  Infraestrutura.Persistencia.RotinasDeBancoDeDados;

{ TRepositorioDeClientesMySQL }

constructor TRepositorioDeClientesMySQL.Create(const AFabricaDeBancoDeDados: IFabricaDeBancoDeDados);
begin
  FFabricaDeBancoDeDados := AFabricaDeBancoDeDados;
  FRepositorioDeContatos := TRepositorioDeContatosMySQL.Create;
end;

destructor TRepositorioDeClientesMySQL.Destroy;
begin
  FreeAndNil(FRepositorioDeContatos);
  inherited;
end;

procedure TRepositorioDeClientesMySQL.InserirCliente(const AClienteMySQL: TClienteMySQL;
  const ABancoDeDados: IBancoDeDados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('INSERT INTO ' + FCliente + ' (');
    lSQL.Add('    ' + FNomeFantasia);
    lSQL.Add('  , ' + FRazaoSocial);
    lSQL.Add('  , ' + FCPFJ);
    lSQL.Add('  , ' + FDataDeInsercao);
    lSQL.Add(') VALUES (');
    lSQL.Add('    ' + QuotedStr(AClienteMySQL.NomeFantasia));
    lSQL.Add('  , ' + QuotedStr(AClienteMySQL.RazaoSocial));
    lSQL.Add('  , ' + QuotedStr(AClienteMySQL.CPFJ));
    lSQL.Add('  , ' + QuotedStr(TRotinasDeBancoDeDados.DataHoraParaTexto(AClienteMySQL.DataDeInsercao)));
    lSQL.Add(')');
    ABancoDeDados.IniciarTransacao;
    try
      AClienteMySQL.IDCliente := ABancoDeDados.Executar(lSQL.Text);
      FRepositorioDeContatos.InserirContatos(AClienteMySQL.Contatos, ABancoDeDados);
      ABancoDeDados.ConfirmarTransacao;
    except
      ABancoDeDados.CancelarTransacao;
      raise;
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

procedure TRepositorioDeClientesMySQL.AlterarCliente(const AClienteMySQL: TClienteMySQL;
  const ABancoDeDados: IBancoDeDados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('UPDATE ' + FCliente);
    lSQL.Add('   SET ' + FNomeFantasia + ' = ' + QuotedStr(AClienteMySQL.NomeFantasia));
    lSQL.Add('     , ' + FRazaoSocial + ' = ' + QuotedStr(AClienteMySQL.RazaoSocial));
    lSQL.Add('     , ' + FCPFJ + ' = ' + QuotedStr(AClienteMySQL.CPFJ));
    lSQL.Add('     , ' + FDataDeInsercao + ' = ' + QuotedStr(TRotinasDeBancoDeDados.DataHoraParaTexto(AClienteMySQL.DataDeInsercao)));
    lSQL.Add('     , ' + FDataDeAlteracao + ' = ' + QuotedStr(TRotinasDeBancoDeDados.DataHoraParaTexto(AClienteMySQL.DataDeAlteracao)));
    lSQL.Add(' WHERE ' + FIDCliente + ' = ' + IntToStr(AClienteMySQL.IDCliente));
    ABancoDeDados.IniciarTransacao;
    try
      ABancoDeDados.Executar(lSQL.Text);
      FRepositorioDeContatos.AlterarContatos(AClienteMySQL.Contatos, ABancoDeDados);
      ABancoDeDados.ConfirmarTransacao;
    except
      ABancoDeDados.CancelarTransacao;
      raise;
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

procedure TRepositorioDeClientesMySQL.ExcluirCliente(const AClienteMySQL: TClienteMySQL;
  const ABancoDeDados: IBancoDedados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('DELETE FROM ' + FCliente);
    lSQL.Add('      WHERE ' + FIDCliente + ' = ' + IntToStr(AClienteMySQL.IDCliente));
    ABancoDeDados.IniciarTransacao;
    try
      FRepositorioDeContatos.ExcluirContatos(AClienteMySQL.Contatos, ABancoDeDados);
      ABancoDeDados.Executar(lSQL.Text);
      ABancoDeDados.ConfirmarTransacao;
    except
      ABancoDeDados.CancelarTransacao;
      raise;
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

function TRepositorioDeClientesMySQL.CarregarClientes(const ANomeFantasia: string;
  const ABancoDeDados: IBancoDeDados): TObjectList<TClienteMySQL>;
var
  lSQL: TStrings;
  lConsulta: TDataSet;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('SELECT ' + FIDCliente);
    lSQL.Add('     , ' + FNomeFantasia);
    lSQL.Add('     , ' + FRazaoSocial);
    lSQL.Add('     , ' + FCPFJ);
    lSQL.Add('     , ' + FDataDeInsercao);
    lSQL.Add('     , ' + FDataDeAlteracao);
    lSQL.Add('  FROM ' + FCliente);
    if ANomeFantasia <> '' then
      lSQL.Add(' WHERE ' + FNomeFantasia + ' LIKE ' + QuotedStr(ANomeFantasia + '%'));
    lConsulta := ABancoDeDados.Consultar(lSQL.Text);
    try
      Result := LerClientes(lConsulta, ABancoDeDados);
    finally
      FreeAndNil(lConsulta);
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

function TRepositorioDeClientesMySQL.CarregarCliente(const ACPFJ: string;
  const ABancoDeDados: IBancoDeDados): TClienteMySQL;
var
  lSQL: TStrings;
  lConsulta: TDataSet;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('SELECT ' + FIDCliente);
    lSQL.Add('     , ' + FNomeFantasia);
    lSQL.Add('     , ' + FRazaoSocial);
    lSQL.Add('     , ' + FCPFJ);
    lSQL.Add('     , ' + FDataDeInsercao);
    lSQL.Add('     , ' + FDataDeAlteracao);
    lSQL.Add('  FROM ' + FCliente);
    lSQL.Add(' WHERE ' + FCPFJ + ' = ' + QuotedStr(ACPFJ));
    lConsulta := ABancoDeDados.Consultar(lSQL.Text);
    try
      if not lConsulta.IsEmpty then
        Result := LerCliente(lConsulta, ABancoDeDados)
      else
        Result := nil;
    finally
      FreeAndNil(lConsulta);
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

function TRepositorioDeClientesMySQL.LerClientes(const ADataSet: TDataSet;
  const ABancoDeDados: IBancoDeDados): TObjectList<TClienteMySQL>;
begin
  Result := TObjectList<TClienteMySQL>.Create(True);
  try
    while not ADataSet.Eof do
    begin
      Result.Add(LerCliente(ADataSet, ABancoDeDados));
      ADataSet.Next;
    end;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TRepositorioDeClientesMySQL.LerCliente(const ADataSet: TDataSet;
  const ABancoDeDados: IBancoDeDados): TClienteMySQL;
begin
  Result := TClienteMySQL.Create;
  try
    Result.IDCliente := ADataSet.FieldByName(FIDCliente).AsInteger;
    Result.NomeFantasia := ADataSet.FieldByName(FNomeFantasia).AsString;
    Result.RazaoSocial := ADataSet.FieldByName(FRazaoSocial).AsString;
    Result.CPFJ := ADataSet.FieldByName(FCPFJ).AsString;
    Result.DataDeInsercao := ADataSet.FieldByName(FDataDeInsercao).AsDateTime;
    Result.DataDeAlteracao := ADataSet.FieldByName(FDataDeAlteracao).AsDateTime;
    Result.Contatos := FRepositorioDeContatos.CarregarContatos(Result.IDCliente, ABancoDeDados);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TRepositorioDeClientesMySQL.Inserir(const ACliente: TCliente): TCliente;
var
  lClienteMySQL: TClienteMySQL;
  lBancoDeDados: IBancoDeDados;
begin
  lClienteMySQL := TClienteMySQL.ConverterParaMySQL(ACliente);
  try
    lClienteMySQL.DataDeInsercao := Now;
    lBancoDeDados := FFabricaDeBancoDeDados.Criar;
    InserirCliente(lClienteMySQL, lBancoDeDados);
    Result := TClienteMySQL.ConverterParaModelo(lClienteMySQL);
  finally
    FreeAndNil(lClienteMySQL);
  end;
end;

function TRepositorioDeClientesMySQL.Alterar(const ACliente: TCliente): TCliente;
var
  lBancoDeDados: IBancoDeDados;
  lClienteMySQLExistente: TClienteMySQL;
  lClienteMySQL: TClienteMySQL;
begin
  lBancoDeDados := FFabricaDeBancoDeDados.Criar;
  lClienteMySQLExistente := CarregarCliente(ACliente.CPFJ.Valor, lBancoDeDados);
  if Assigned(lClienteMySQLExistente) then
    try
      lClienteMySQL := TClienteMySQL.ConverterParaMySQL(ACliente);
      try
        lClienteMySQL.IDCliente := lClienteMySQLExistente.IDCliente;
        lClienteMySQL.DataDeInsercao := lClienteMySQLExistente.DataDeInsercao;
        lClienteMySQL.DataDeAlteracao := Now;
        AlterarCliente(lClienteMySQL, lBancoDeDados);
        Result := TClienteMySQL.ConverterParaModelo(lClienteMySQL);
      finally
        FreeAndNil(lClienteMySQL);
      end;
    finally
      FreeAndNil(lClienteMySQLExistente);
    end
  else
    Result := nil;
end;

function TRepositorioDeClientesMySQL.Excluir(const ACPFJ: TCPFJ): TCliente;
var
  lBancoDeDados: IBancoDeDados;
  lClienteMySQL: TClienteMySQL;
begin
  lBancoDeDados := FFabricaDeBancoDeDados.Criar;
  lClienteMySQL := CarregarCliente(ACPFJ.Valor, lBancoDeDados);
  if Assigned(lClienteMySQL) then
    try
      ExcluirCliente(lClienteMySQL, lBancoDeDados);
      Result := TClienteMySQL.ConverterParaModelo(lClienteMySQL);
    finally
      FreeAndNil(lClienteMySQL);
    end
  else
    Result := nil;
end;

function TRepositorioDeClientesMySQL.Buscar(const ACPFJ: TCPFJ): TCliente;
var
  lBancoDeDados: IBancoDeDados;
  lClienteMySQL: TClienteMySQL;
begin
  lBancoDeDados := FFabricaDeBancoDeDados.Criar;
  lClienteMySQL := CarregarCliente(ACPFJ.Valor, lBancoDeDados);
  if Assigned(lClienteMySQL) then
    try
      Result := TClienteMySQL.ConverterParaModelo(lClienteMySQL);
    finally
      FreeAndNil(lClienteMySQL);
    end
  else
    Result := nil;
end;

function TRepositorioDeClientesMySQL.Consultar(const ANomeFantasia: string): TObjectList<TCliente>;
var
  lBancoDeDados: IBancoDeDados;
  lClientesMySQL: TObjectList<TClienteMySQL>;
begin
  lBancoDeDados := FFabricaDeBancoDeDados.Criar;
  lClientesMySQL := CarregarClientes(ANomeFantasia, lBancoDeDados);
  try
    Result := TClienteMySQL.ConverterParaModelo(lClientesMySQL);
  finally
    FreeAndNil(lClientesMySQL);
  end;
end;

{ TRepositorioDeContatosMySQL }

constructor TRepositorioDeContatosMySQL.Create;
begin
  FRepositorioDeTelefones := TRepositorioDeTelefonesMySQL.Create;
end;

destructor TRepositorioDeContatosMySQL.Destroy;
begin
  FreeAndNil(FRepositorioDeTelefones);
  inherited;
end;

procedure TRepositorioDeContatosMySQL.InserirContatos(const AContatosMySQL: TObjectList<TContatoMySQL>;
  const ABancoDeDados: IBancoDeDados);
var
  I: Integer;
begin
  ABancoDeDados.IniciarTransacao;
  try
    for I := 0 to Pred(AContatosMySQL.Count) do
      InserirContato(AContatosMySQL[I], ABancoDeDados);
    ABancoDeDados.ConfirmarTransacao;
  except
    ABancoDeDados.CancelarTransacao;
    raise;
  end;
end;

procedure TRepositorioDeContatosMySQL.InserirContato(const AContatoMySQL: TContatoMySQL;
  const ABancoDeDados: IBancoDeDados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('INSERT INTO ' + FContato + ' (');
    lSQL.Add('    ' + FIDCliente);
    lSQL.Add('  , ' + FNome);
    lSQL.Add(') VALUES (');
    lSQL.Add('    ' + IntToStr(AContatoMySQL.Cliente.IDCliente));
    lSQL.Add('  , ' + QuotedStr(AContatoMySQL.Nome));
    lSQL.Add(')');
    ABancoDeDados.IniciarTransacao;
    try
      AContatoMySQL.IDContato := ABancoDeDados.Executar(lSQL.Text);
      FRepositorioDeTelefones.InserirTelefones(AContatoMySQL.Telefones, ABancoDeDados);
      ABancoDeDados.ConfirmarTransacao;
    except
      ABancoDeDados.CancelarTransacao;
      raise;
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

procedure TRepositorioDeContatosMySQL.AlterarContatos(const AContatosMySQL: TObjectList<TContatoMySQL>;
  const ABancoDeDados: IBancoDeDados);
var
  lContatosMySQL: TObjectList<TContatoMySQL>;
  lIDs: TDictionary<Integer, TContatoMySQL>;
  I: Integer;
begin
  lIDs := TDictionary<Integer, TContatoMySQL>.Create;
  try
    for I := 0 to Pred(AContatosMySQL.Count) do
      if AContatosMySQL[I].IDContato > 0 then
        lIDs.Add(AContatosMySQL[I].IDContato, AContatosMySQL[I]);
    lContatosMySQL := CarregarContatos(AContatosMySQL[0].Cliente.IDCliente, ABancoDeDados);
    try
      ABancoDeDados.IniciarTransacao;
      try
        for I := 0 to Pred(lContatosMySQL.Count) do
          if not lIDs.ContainsKey(lContatosMySQL[I].IDContato) then
            ExcluirContato(lContatosMySQL[I], ABancoDeDados);
        for I := 0 to Pred(AContatosMySQL.Count) do
          if AContatosMySQL[I].IDContato = 0 then
            InserirContato(AContatosMySQL[I], ABancoDeDados)
          else
            AlterarContato(AContatosMySQL[I], ABancoDeDados);
        ABancoDeDados.ConfirmarTransacao;
      except
        ABancoDeDados.CancelarTransacao;
        raise;
      end;
    finally
      FreeAndNil(lContatosMySQL);
    end;
  finally
    FreeAndNil(lIDs);
  end;
end;

procedure TRepositorioDeContatosMySQL.AlterarContato(const AContatoMySQL: TContatoMySQL;
  const ABancoDeDados: IBancoDeDados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('UPDATE ' + FContato);
    lSQL.Add('   SET ' + FIDCliente + ' = ' + IntToStr(AContatoMySQL.Cliente.IDCliente));
    lSQL.Add('     , ' + FNome + ' = ' + QuotedStr(AContatoMySQL.Nome));
    lSQL.Add(' WHERE ' + FIDContato + ' = ' + IntToStr(AContatoMySQL.IDContato));
    ABancoDeDados.IniciarTransacao;
    try
      ABancoDeDados.Executar(lSQL.Text);
      FRepositorioDeTelefones.AlterarTelefones(AContatoMySQL.Telefones, ABancoDeDados);
      ABancoDeDados.ConfirmarTransacao;
    except
      ABancoDeDados.CancelarTransacao;
      raise;
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

procedure TRepositorioDeContatosMySQL.ExcluirContatos(const AContatosMySQL: TObjectList<TContatoMySQL>;
  const ABancoDeDados: IBancoDedados);
var
  I: Integer;
begin
  ABancoDeDados.IniciarTransacao;
  try
    for I := 0 to Pred(AContatosMySQL.Count) do
      ExcluirContato(AContatosMySQL[I], ABancoDeDados);
    ABancoDeDados.ConfirmarTransacao;
  except
    ABancoDeDados.CancelarTransacao;
    raise;
  end;
end;

procedure TRepositorioDeContatosMySQL.ExcluirContato(const AContatoMySQL: TContatoMySQL;
  const ABancoDeDados: IBancoDedados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('DELETE FROM ' + FContato);
    lSQL.Add('      WHERE ' + FIDContato + ' = ' + IntToStr(AContatoMySQL.IDContato));
    ABancoDeDados.IniciarTransacao;
    try
      FRepositorioDeTelefones.ExcluirTelefones(AContatoMySQL.Telefones, ABancoDeDados);
      ABancoDeDados.Executar(lSQL.Text);
      ABancoDeDados.ConfirmarTransacao;
    except
      ABancoDeDados.CancelarTransacao;
      raise;
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

function TRepositorioDeContatosMySQL.CarregarContatos(const AIDCliente: Integer;
  const ABancoDeDados: IBancoDeDados): TObjectList<TContatoMySQL>;
var
  lSQL: TStrings;
  lConsulta: TDataSet;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('SELECT ' + FIDContato);
    lSQL.Add('     , ' + FIDCliente);
    lSQL.Add('     , ' + FNome);
    lSQL.Add('  FROM ' + FContato);
    lSQL.Add(' WHERE ' + FIDCliente + ' = ' + IntToStr(AIDCliente));
    lConsulta := ABancoDeDados.Consultar(lSQL.Text);
    try
      Result := LerContatos(lConsulta, ABancoDeDados);
    finally
      FreeAndNil(lConsulta);
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

function TRepositorioDeContatosMySQL.LerContatos(const ADataSet: TDataSet;
  const ABancoDeDados: IBancoDeDados): TObjectList<TContatoMySQL>;
begin
  Result := TObjectList<TContatoMySQL>.Create(True);
  try
    while not ADataSet.Eof do
    begin
      Result.Add(LerContato(ADataSet, ABancoDeDados));
      ADataSet.Next;
    end;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TRepositorioDeContatosMySQL.LerContato(const ADataSet: TDataSet;
  const ABancoDeDados: IBancoDeDados): TContatoMySQL;
begin
  Result := TContatoMySQL.Create;
  try
    Result.IDContato := ADataSet.FieldByName(FIDContato).AsInteger;
    Result.Nome := ADataSet.FieldByName(FNome).AsString;
    Result.Telefones := FRepositorioDeTelefones.CarregarTelefones(Result.IDContato, ABancoDeDados);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

{ TRepositorioDeTelefonesMySQL }

procedure TRepositorioDeTelefonesMySQL.InserirTelefones(const ATelefonesMySQL: TObjectList<TTelefoneMySQL>;
  const ABancoDeDados: IBancoDeDados);
var
  I: Integer;
begin
  ABancoDeDados.IniciarTransacao;
  try
    for I := 0 to Pred(ATelefonesMySQL.Count) do
      InserirTelefone(ATelefonesMySQL[I], ABancoDeDados);
    ABancoDeDados.ConfirmarTransacao;
  except
    ABancoDeDados.CancelarTransacao;
    raise;
  end;
end;

procedure TRepositorioDeTelefonesMySQL.InserirTelefone(const ATelefoneMySQL: TTelefoneMySQL;
  const ABancoDeDados: IBancoDeDados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('INSERT INTO ' + FTelefone + ' (');
    lSQL.Add('    ' + FIDContato);
    lSQL.Add('  , ' + FDescricao);
    lSQL.Add('  , ' + FNumero);
    lSQL.Add(') VALUES (');
    lSQL.Add('    ' + IntToStr(Integer(ATelefoneMySQL.Contato.IDContato)));
    lSQL.Add('  , ' + QuotedStr(ATelefoneMySQL.Descricao));
    lSQL.Add('  , ' + QuotedStr(ATelefoneMySQL.Numero));
    lSQL.Add(')');
    ATelefoneMySQL.IDTelefone := ABancoDeDados.Executar(lSQL.Text);
  finally
    FreeAndNil(lSQL);
  end;
end;

procedure TRepositorioDeTelefonesMySQL.AlterarTelefones(const ATelefonesMySQL: TObjectList<TTelefoneMySQL>;
  const ABancoDeDados: IBancoDeDados);
var
  lTelefonesMySQL: TObjectList<TTelefoneMySQL>;
  lIDs: TDictionary<Integer, TTelefoneMySQL>;
  I: Integer;
begin
  lIDs := TDictionary<Integer, TTelefoneMySQL>.Create;
  try
    for I := 0 to Pred(ATelefonesMySQL.Count) do
      if ATelefonesMySQL[I].IDTelefone > 0 then
        lIDs.Add(ATelefonesMySQL[I].IDTelefone, ATelefonesMySQL[I]);
    lTelefonesMySQL := CarregarTelefones(ATelefonesMySQL[0].Contato.IDContato, ABancoDeDados);
    try
      ABancoDeDados.IniciarTransacao;
      try
        for I := 0 to Pred(lTelefonesMySQL.Count) do
          if not lIDs.ContainsKey(lTelefonesMySQL[I].IDTelefone) then
            ExcluirTelefone(lTelefonesMySQL[I], ABancoDeDados);
        for I := 0 to Pred(ATelefonesMySQL.Count) do
          if ATelefonesMySQL[I].IDTelefone = 0 then
            InserirTelefone(ATelefonesMySQL[I], ABancoDeDados)
          else
            AlterarTelefone(ATelefonesMySQL[I], ABancoDeDados);
        ABancoDeDados.ConfirmarTransacao;
      except
        ABancoDeDados.CancelarTransacao;
        raise;
      end;
    finally
      FreeAndNil(lTelefonesMySQL);
    end;
  finally
    FreeAndNil(lIDs);
  end;
end;

procedure TRepositorioDeTelefonesMySQL.AlterarTelefone(const ATelefoneMySQL: TTelefoneMySQL;
  const ABancoDeDados: IBancoDeDados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('UPDATE ' + FTelefone);
    lSQL.Add('   SET ' + FIDContato + ' = ' + IntToStr(ATelefoneMySQL.Contato.IDContato));
    lSQL.Add('     , ' + FDescricao + ' = ' + QuotedStr(ATelefoneMySQL.Numero));
    lSQL.Add('     , ' + FNumero + ' = ' + QuotedStr(ATelefoneMySQL.Numero));
    lSQL.Add(' WHERE ' + FIDTelefone + ' = ' + IntToStr(ATelefoneMySQL.IDTelefone));
    ABancoDeDados.Executar(lSQL.Text);
  finally
    FreeAndNil(lSQL);
  end;
end;

procedure TRepositorioDeTelefonesMySQL.ExcluirTelefones(const ATelefonesMySQL: TObjectList<TTelefoneMySQL>;
  const ABancoDeDados: IBancoDedados);
var
  I: Integer;
begin
  ABancoDeDados.IniciarTransacao;
  try
    for I := 0 to Pred(ATelefonesMySQL.Count) do
      ExcluirTelefone(ATelefonesMySQL[I], ABancoDeDados);
    ABancoDeDados.ConfirmarTransacao;
  except
    ABancoDeDados.CancelarTransacao;
    raise;
  end;
end;

procedure TRepositorioDeTelefonesMySQL.ExcluirTelefone(const ATelefoneMySQL: TTelefoneMySQL;
  const ABancoDeDados: IBancoDedados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('DELETE FROM ' + FTelefone);
    lSQL.Add('      WHERE ' + FIDTelefone + ' = ' + IntToStr(ATelefoneMySQL.IDTelefone));
    ABancoDeDados.Executar(lSQL.Text);
  finally
    FreeAndNil(lSQL);
  end;
end;

function TRepositorioDeTelefonesMySQL.CarregarTelefones(const AIDContato: Integer;
  const ABancoDeDados: IBancoDeDados): TObjectList<TTelefoneMySQL>;
var
  lSQL: TStrings;
  lConsulta: TDataSet;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('SELECT ' + FIDTelefone);
    lSQL.Add('     , ' + FIDContato);
    lSQL.Add('     , ' + FDescricao);
    lSQL.Add('     , ' + FNumero);
    lSQL.Add('  FROM ' + FTelefone);
    lSQL.Add(' WHERE ' + FIDContato + ' = ' + IntToStr(AIDContato));
    lConsulta := ABancoDeDados.Consultar(lSQL.Text);
    try
      Result := LerTelefones(lConsulta);
    finally
      FreeAndNil(lConsulta);
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

function TRepositorioDeTelefonesMySQL.LerTelefones(const ADataSet: TDataSet): TObjectList<TTelefoneMySQL>;
begin
  Result := TObjectList<TTelefoneMySQL>.Create(True);
  try
    while not ADataSet.Eof do
    begin
      Result.Add(LerTelefone(ADataSet));
      ADataSet.Next;
    end;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TRepositorioDeTelefonesMySQL.LerTelefone(const ADataSet: TDataSet): TTelefoneMySQL;
begin
  Result := TTelefoneMySQL.Create;
  try
    Result.IDTelefone := ADataSet.FieldByName(FIDTelefone).AsInteger;
    Result.Descricao := ADataSet.FieldByName(FDescricao).AsString;
    Result.Numero := ADataSet.FieldByName(FNumero).AsString;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

end.
