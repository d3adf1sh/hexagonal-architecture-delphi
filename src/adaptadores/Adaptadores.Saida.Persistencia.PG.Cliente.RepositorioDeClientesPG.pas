unit Adaptadores.Saida.Persistencia.PG.Cliente.RepositorioDeClientesPG;

interface

uses
  System.SysUtils,
  System.Generics.Collections,
  Data.DB,
  Infraestrutura.Persistencia.BancoDeDados,
  Infraestrutura.Persistencia.FabricaDeBancoDeDados,
  Modelo.Entidades.Cliente,
  Modelo.Valores.CPFJ,
  Aplicacao.Portas.Saida.RepositorioDeClientes,
  Adaptadores.Saida.Persistencia.PG.Cliente.ClientePG;

type
  //Forward declarations
  TRepositorioDeContatosPG = class;
  TRepositorioDeTelefonesPG = class;

  //TRepositorioDeClientesPG
  TRepositorioDeClientesPG = class sealed(TInterfacedObject, IRepositorioDeClientes)
  private const
    FCliente = 'public.cliente';
    FIDCliente = 'id_cliente';
    FNomeFantasia  = 'nome_fantasia';
    FRazaoSocial = 'razao_social';
    FCPFJ = 'cpfj';
    FDataDeInsercao = 'data_de_insercao';
    FDataDeAlteracao = 'data_de_alteracao';
  private
    FFabricaDeBancoDeDados: IFabricaDeBancoDeDados;
    FRepositorioDeContatos: TRepositorioDeContatosPG;
    procedure InserirCliente(const AClientePG: TClientePG; const ABancoDeDados: IBancoDeDados);
    procedure AlterarCliente(const AClientePG: TClientePG; const ABancoDeDados: IBancoDeDados);
    procedure ExcluirCliente(const AClientePG: TClientePG; const ABancoDeDados: IBancoDedados);
    function CarregarClientes(const ANomeFantasia: string; const ABancoDeDados: IBancoDeDados): TObjectList<TClientePG>;
    function CarregarCliente(const ACPFJ: string; const ABancoDeDados: IBancoDeDados): TClientePG;
    function LerClientes(const ADataSet: TDataSet; const ABancoDeDados: IBancoDeDados): TObjectList<TClientePG>;
    function LerCliente(const ADataSet: TDataSet; const ABancoDeDados: IBancoDeDados): TClientePG;
  public
    constructor Create(const AFabricaDeBancoDeDados: IFabricaDeBancoDeDados);
    destructor Destroy; override;
    function Inserir(const ACliente: TCliente): TCliente;
    function Alterar(const ACliente: TCliente): TCliente;
    function Excluir(const ACPFJ: TCPFJ): TCliente;
    function Buscar(const ACPFJ: TCPFJ): TCliente;
    function Consultar(const ANomeFantasia: string): TObjectList<TCliente>;
  end;

  //TRepositorioDeContatosPG
  TRepositorioDeContatosPG = class sealed
  private const
    FContato = 'public.contato';
    FIDContato = 'id_contato';
    FIDCliente = 'id_cliente';
    FNome = 'nome';
  private
    FRepositorioDeTelefones: TRepositorioDeTelefonesPG;
    procedure InserirContatos(const AContatosPG: TObjectList<TContatoPG>; const ABancoDeDados: IBancoDeDados);
    procedure InserirContato(const AContatoPG: TContatoPG; const ABancoDeDados: IBancoDeDados);
    procedure AlterarContatos(const AContatosPG: TObjectList<TContatoPG>; const ABancoDeDados: IBancoDeDados);
    procedure AlterarContato(const AContatoPG: TContatoPG; const ABancoDeDados: IBancoDeDados);
    procedure ExcluirContatos(const AContatosPG: TObjectList<TContatoPG>; const ABancoDeDados: IBancoDedados);
    procedure ExcluirContato(const AContatoPG: TContatoPG; const ABancoDeDados: IBancoDedados);
    function CarregarContatos(const AIDCliente: Integer; const ABancoDeDados: IBancoDeDados): TObjectList<TContatoPG>;
    function LerContatos(const ADataSet: TDataSet; const ABancoDeDados: IBancoDeDados): TObjectList<TContatoPG>;
    function LerContato(const ADataSet: TDataSet; const ABancoDeDados: IBancoDeDados): TContatoPG;
  public
    constructor Create;
    destructor Destroy; override;
  end;

  //TRepositorioDeTelefonesPG
  TRepositorioDeTelefonesPG = class sealed
  private const
    FTelefone = 'public.telefone';
    FIDTelefone = 'id_telefone';
    FIDContato = 'id_contato';
    FDescricao = 'descricao';
    FNumero = 'numero';
  private
    procedure InserirTelefones(const ATelefonesPG: TObjectList<TTelefonePG>; const ABancoDeDados: IBancoDeDados);
    procedure InserirTelefone(const ATelefonePG: TTelefonePG; const ABancoDeDados: IBancoDeDados);
    procedure AlterarTelefones(const ATelefonesPG: TObjectList<TTelefonePG>; const ABancoDeDados: IBancoDeDados);
    procedure AlterarTelefone(const ATelefonePG: TTelefonePG; const ABancoDeDados: IBancoDeDados);
    procedure ExcluirTelefones(const ATelefonesPG: TObjectList<TTelefonePG>; const ABancoDeDados: IBancoDedados);
    procedure ExcluirTelefone(const ATelefonePG: TTelefonePG; const ABancoDeDados: IBancoDedados);
    function CarregarTelefones(const AIDContato: Integer; const ABancoDeDados: IBancoDeDados): TObjectList<TTelefonePG>;
    function LerTelefones(const ADataSet: TDataSet): TObjectList<TTelefonePG>;
    function LerTelefone(const ADataSet: TDataSet): TTelefonePG;
  end;

implementation

uses
  System.Classes,
  Infraestrutura.Persistencia.RotinasDeBancoDeDados;

{ TRepositorioDeClientesPG }

constructor TRepositorioDeClientesPG.Create(const AFabricaDeBancoDeDados: IFabricaDeBancoDeDados);
begin
  FFabricaDeBancoDeDados := AFabricaDeBancoDeDados;
  FRepositorioDeContatos := TRepositorioDeContatosPG.Create;
end;

destructor TRepositorioDeClientesPG.Destroy;
begin
  FreeAndNil(FRepositorioDeContatos);
  inherited;
end;

procedure TRepositorioDeClientesPG.InserirCliente(const AClientePG: TClientePG; const ABancoDeDados: IBancoDeDados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('INSERT INTO ' + FCliente + ' (');
    lSQL.Add('    ' + FNomeFantasia);
    lSQL.Add('  , ' + FRazaoSocial);
    lSQL.Add('  , ' + FCPFJ);
    lSQL.Add('  , ' + FDataDeInsercao);
    lSQL.Add(') VALUES (');
    lSQL.Add('    ' + QuotedStr(AClientePG.NomeFantasia));
    lSQL.Add('  , ' + QuotedStr(AClientePG.RazaoSocial));
    lSQL.Add('  , ' + QuotedStr(AClientePG.CPFJ));
    lSQL.Add('  , ' + QuotedStr(TRotinasDeBancoDeDados.DataHoraParaTexto(AClientePG.DataDeInsercao)));
    lSQL.Add(') RETURNING ' + FIDCliente + ' {INTO :id}');
    ABancoDeDados.IniciarTransacao;
    try
      AClientePG.IDCliente := ABancoDeDados.Executar(lSQL.Text);
      FRepositorioDeContatos.InserirContatos(AClientePG.Contatos, ABancoDeDados);
      ABancoDeDados.ConfirmarTransacao;
    except
      ABancoDeDados.CancelarTransacao;
      raise;
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

procedure TRepositorioDeClientesPG.AlterarCliente(const AClientePG: TClientePG; const ABancoDeDados: IBancoDeDados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('UPDATE ' + FCliente);
    lSQL.Add('   SET ' + FNomeFantasia + ' = ' + QuotedStr(AClientePG.NomeFantasia));
    lSQL.Add('     , ' + FRazaoSocial + ' = ' + QuotedStr(AClientePG.RazaoSocial));
    lSQL.Add('     , ' + FCPFJ + ' = ' + QuotedStr(AClientePG.CPFJ));
    lSQL.Add('     , ' + FDataDeInsercao + ' = ' + QuotedStr(TRotinasDeBancoDeDados.DataHoraParaTexto(AClientePG.DataDeInsercao)));
    lSQL.Add('     , ' + FDataDeAlteracao + ' = ' + QuotedStr(TRotinasDeBancoDeDados.DataHoraParaTexto(AClientePG.DataDeAlteracao)));
    lSQL.Add(' WHERE ' + FIDCliente + ' = ' + IntToStr(AClientePG.IDCliente));
    ABancoDeDados.IniciarTransacao;
    try
      ABancoDeDados.Executar(lSQL.Text);
      FRepositorioDeContatos.AlterarContatos(AClientePG.Contatos, ABancoDeDados);
      ABancoDeDados.ConfirmarTransacao;
    except
      ABancoDeDados.CancelarTransacao;
      raise;
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

procedure TRepositorioDeClientesPG.ExcluirCliente(const AClientePG: TClientePG; const ABancoDeDados: IBancoDedados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('DELETE FROM ' + FCliente);
    lSQL.Add('      WHERE ' + FIDCliente + ' = ' + IntToStr(AClientePG.IDCliente));
    ABancoDeDados.IniciarTransacao;
    try
      FRepositorioDeContatos.ExcluirContatos(AClientePG.Contatos, ABancoDeDados);
      ABancoDeDados.Executar(lSQL.Text);
      ABancoDeDados.ConfirmarTransacao;
    except
      ABancoDeDados.CancelarTransacao;
      raise;
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

function TRepositorioDeClientesPG.CarregarClientes(const ANomeFantasia: string;
  const ABancoDeDados: IBancoDeDados): TObjectList<TClientePG>;
var
  lSQL: TStrings;
  lConsulta: TDataSet;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('SELECT ' + FIDCliente);
    lSQL.Add('     , ' + FNomeFantasia);
    lSQL.Add('     , ' + FRazaoSocial);
    lSQL.Add('     , ' + FCPFJ);
    lSQL.Add('     , ' + FDataDeInsercao);
    lSQL.Add('     , ' + FDataDeAlteracao);
    lSQL.Add('  FROM ' + FCliente);
    if ANomeFantasia <> '' then
      lSQL.Add(' WHERE ' + FNomeFantasia + ' LIKE ' + QuotedStr(ANomeFantasia + '%'));
    lConsulta := ABancoDeDados.Consultar(lSQL.Text);
    try
      Result := LerClientes(lConsulta, ABancoDeDados);
    finally
      FreeAndNil(lConsulta);
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

function TRepositorioDeClientesPG.CarregarCliente(const ACPFJ: string; const ABancoDeDados: IBancoDeDados): TClientePG;
var
  lSQL: TStrings;
  lConsulta: TDataSet;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('SELECT ' + FIDCliente);
    lSQL.Add('     , ' + FNomeFantasia);
    lSQL.Add('     , ' + FRazaoSocial);
    lSQL.Add('     , ' + FCPFJ);
    lSQL.Add('     , ' + FDataDeInsercao);
    lSQL.Add('     , ' + FDataDeAlteracao);
    lSQL.Add('  FROM ' + FCliente);
    lSQL.Add(' WHERE ' + FCPFJ + ' = ' + QuotedStr(ACPFJ));
    lConsulta := ABancoDeDados.Consultar(lSQL.Text);
    try
      if not lConsulta.IsEmpty then
        Result := LerCliente(lConsulta, ABancoDeDados)
      else
        Result := nil;
    finally
      FreeAndNil(lConsulta);
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

function TRepositorioDeClientesPG.LerClientes(const ADataSet: TDataSet;
  const ABancoDeDados: IBancoDeDados): TObjectList<TClientePG>;
begin
  Result := TObjectList<TClientePG>.Create(True);
  try
    while not ADataSet.Eof do
    begin
      Result.Add(LerCliente(ADataSet, ABancoDeDados));
      ADataSet.Next;
    end;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TRepositorioDeClientesPG.LerCliente(const ADataSet: TDataSet; const ABancoDeDados: IBancoDeDados): TClientePG;
begin
  Result := TClientePG.Create;
  try
    Result.IDCliente := ADataSet.FieldByName(FIDCliente).AsInteger;
    Result.NomeFantasia := ADataSet.FieldByName(FNomeFantasia).AsString;
    Result.RazaoSocial := ADataSet.FieldByName(FRazaoSocial).AsString;
    Result.CPFJ := ADataSet.FieldByName(FCPFJ).AsString;
    Result.DataDeInsercao := ADataSet.FieldByName(FDataDeInsercao).AsDateTime;
    Result.DataDeAlteracao := ADataSet.FieldByName(FDataDeAlteracao).AsDateTime;
    Result.Contatos := FRepositorioDeContatos.CarregarContatos(Result.IDCliente, ABancoDeDados);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TRepositorioDeClientesPG.Inserir(const ACliente: TCliente): TCliente;
var
  lClientePG: TClientePG;
  lBancoDeDados: IBancoDeDados;
begin
  lClientePG := TClientePG.ConverterParaPG(ACliente);
  try
    lClientePG.DataDeInsercao := Now;
    lBancoDeDados := FFabricaDeBancoDeDados.Criar;
    InserirCliente(lClientePG, lBancoDeDados);
    Result := TClientePG.ConverterParaModelo(lClientePG);
  finally
    FreeAndNil(lClientePG);
  end;
end;

function TRepositorioDeClientesPG.Alterar(const ACliente: TCliente): TCliente;
var
  lBancoDeDados: IBancoDeDados;
  lClientePGExistente: TClientePG;
  lClientePG: TClientePG;
begin
  lBancoDeDados := FFabricaDeBancoDeDados.Criar;
  lClientePGExistente := CarregarCliente(ACliente.CPFJ.Valor, lBancoDeDados);
  if Assigned(lClientePGExistente) then
    try
      lClientePG := TClientePG.ConverterParaPG(ACliente);
      try
        lClientePG.IDCliente := lClientePGExistente.IDCliente;
        lClientePG.DataDeInsercao := lClientePGExistente.DataDeInsercao;
        lClientePG.DataDeAlteracao := Now;
        AlterarCliente(lClientePG, lBancoDeDados);
        Result := TClientePG.ConverterParaModelo(lClientePG);
      finally
        FreeAndNil(lClientePG);
      end;
    finally
      FreeAndNil(lClientePGExistente);
    end
  else
    Result := nil;
end;

function TRepositorioDeClientesPG.Excluir(const ACPFJ: TCPFJ): TCliente;
var
  lBancoDeDados: IBancoDeDados;
  lClientePG: TClientePG;
begin
  lBancoDeDados := FFabricaDeBancoDeDados.Criar;
  lClientePG := CarregarCliente(ACPFJ.Valor, lBancoDeDados);
  if Assigned(lClientePG) then
    try
      ExcluirCliente(lClientePG, lBancoDeDados);
      Result := TClientePG.ConverterParaModelo(lClientePG);
    finally
      FreeAndNil(lClientePG);
    end
  else
    Result := nil;
end;

function TRepositorioDeClientesPG.Buscar(const ACPFJ: TCPFJ): TCliente;
var
  lBancoDeDados: IBancoDeDados;
  lClientePG: TClientePG;
begin
  lBancoDeDados := FFabricaDeBancoDeDados.Criar;
  lClientePG := CarregarCliente(ACPFJ.Valor, lBancoDeDados);
  if Assigned(lClientePG) then
    try
      Result := TClientePG.ConverterParaModelo(lClientePG);
    finally
      FreeAndNil(lClientePG);
    end
  else
    Result := nil;
end;

function TRepositorioDeClientesPG.Consultar(const ANomeFantasia: string): TObjectList<TCliente>;
var
  lBancoDeDados: IBancoDeDados;
  lClientesPG: TObjectList<TClientePG>;
begin
  lBancoDeDados := FFabricaDeBancoDeDados.Criar;
  lClientesPG := CarregarClientes(ANomeFantasia, lBancoDeDados);
  try
    Result := TClientePG.ConverterParaModelo(lClientesPG);
  finally
    FreeAndNil(lClientesPG);
  end;
end;

{ TRepositorioDeContatosPG }

constructor TRepositorioDeContatosPG.Create;
begin
  FRepositorioDeTelefones := TRepositorioDeTelefonesPG.Create;
end;

destructor TRepositorioDeContatosPG.Destroy;
begin
  FreeAndNil(FRepositorioDeTelefones);
  inherited;
end;

procedure TRepositorioDeContatosPG.InserirContatos(const AContatosPG: TObjectList<TContatoPG>;
  const ABancoDeDados: IBancoDeDados);
var
  I: Integer;
begin
  ABancoDeDados.IniciarTransacao;
  try
    for I := 0 to Pred(AContatosPG.Count) do
      InserirContato(AContatosPG[I], ABancoDeDados);
    ABancoDeDados.ConfirmarTransacao;
  except
    ABancoDeDados.CancelarTransacao;
    raise;
  end;
end;

procedure TRepositorioDeContatosPG.InserirContato(const AContatoPG: TContatoPG; const ABancoDeDados: IBancoDeDados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('INSERT INTO ' + FContato + ' (');
    lSQL.Add('    ' + FIDCliente);
    lSQL.Add('  , ' + FNome);
    lSQL.Add(') VALUES (');
    lSQL.Add('    ' + IntToStr(AContatoPG.Cliente.IDCliente));
    lSQL.Add('  , ' + QuotedStr(AContatoPG.Nome));
    lSQL.Add(') RETURNING ' + FIDContato + ' {INTO :id}');
    ABancoDeDados.IniciarTransacao;
    try
      AContatoPG.IDContato := ABancoDeDados.Executar(lSQL.Text);
      FRepositorioDeTelefones.InserirTelefones(AContatoPG.Telefones, ABancoDeDados);
      ABancoDeDados.ConfirmarTransacao;
    except
      ABancoDeDados.CancelarTransacao;
      raise;
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

procedure TRepositorioDeContatosPG.AlterarContatos(const AContatosPG: TObjectList<TContatoPG>;
  const ABancoDeDados: IBancoDeDados);
var
  lContatosPG: TObjectList<TContatoPG>;
  lIDs: TDictionary<Integer, TContatoPG>;
  I: Integer;
begin
  lIDs := TDictionary<Integer, TContatoPG>.Create;
  try
    for I := 0 to Pred(AContatosPG.Count) do
      if AContatosPG[I].IDContato > 0 then
        lIDs.Add(AContatosPG[I].IDContato, AContatosPG[I]);
    lContatosPG := CarregarContatos(AContatosPG[0].Cliente.IDCliente, ABancoDeDados);
    try
      ABancoDeDados.IniciarTransacao;
      try
        for I := 0 to Pred(lContatosPG.Count) do
          if not lIDs.ContainsKey(lContatosPG[I].IDContato) then
            ExcluirContato(lContatosPG[I], ABancoDeDados);
        for I := 0 to Pred(AContatosPG.Count) do
          if AContatosPG[I].IDContato = 0 then
            InserirContato(AContatosPG[I], ABancoDeDados)
          else
            AlterarContato(AContatosPG[I], ABancoDeDados);
        ABancoDeDados.ConfirmarTransacao;
      except
        ABancoDeDados.CancelarTransacao;
        raise;
      end;
    finally
      FreeAndNil(lContatosPG);
    end;
  finally
    FreeAndNil(lIDs);
  end;
end;

procedure TRepositorioDeContatosPG.AlterarContato(const AContatoPG: TContatoPG; const ABancoDeDados: IBancoDeDados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('UPDATE ' + FContato);
    lSQL.Add('   SET ' + FIDCliente + ' = ' + IntToStr(AContatoPG.Cliente.IDCliente));
    lSQL.Add('     , ' + FNome + ' = ' + QuotedStr(AContatoPG.Nome));
    lSQL.Add(' WHERE ' + FIDContato + ' = ' + IntToStr(AContatoPG.IDContato));
    ABancoDeDados.IniciarTransacao;
    try
      ABancoDeDados.Executar(lSQL.Text);
      FRepositorioDeTelefones.AlterarTelefones(AContatoPG.Telefones, ABancoDeDados);
      ABancoDeDados.ConfirmarTransacao;
    except
      ABancoDeDados.CancelarTransacao;
      raise;
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

procedure TRepositorioDeContatosPG.ExcluirContatos(const AContatosPG: TObjectList<TContatoPG>;
  const ABancoDeDados: IBancoDedados);
var
  I: Integer;
begin
  ABancoDeDados.IniciarTransacao;
  try
    for I := 0 to Pred(AContatosPG.Count) do
      ExcluirContato(AContatosPG[I], ABancoDeDados);
    ABancoDeDados.ConfirmarTransacao;
  except
    ABancoDeDados.CancelarTransacao;
    raise;
  end;
end;

procedure TRepositorioDeContatosPG.ExcluirContato(const AContatoPG: TContatoPG; const ABancoDeDados: IBancoDedados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('DELETE FROM ' + FContato);
    lSQL.Add('      WHERE ' + FIDContato + ' = ' + IntToStr(AContatoPG.IDContato));
    ABancoDeDados.IniciarTransacao;
    try
      FRepositorioDeTelefones.ExcluirTelefones(AContatoPG.Telefones, ABancoDeDados);
      ABancoDeDados.Executar(lSQL.Text);
      ABancoDeDados.ConfirmarTransacao;
    except
      ABancoDeDados.CancelarTransacao;
      raise;
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

function TRepositorioDeContatosPG.CarregarContatos(const AIDCliente: Integer;
  const ABancoDeDados: IBancoDeDados): TObjectList<TContatoPG>;
var
  lSQL: TStrings;
  lConsulta: TDataSet;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('SELECT ' + FIDContato);
    lSQL.Add('     , ' + FIDCliente);
    lSQL.Add('     , ' + FNome);
    lSQL.Add('  FROM ' + FContato);
    lSQL.Add(' WHERE ' + FIDCliente + ' = ' + IntToStr(AIDCliente));
    lConsulta := ABancoDeDados.Consultar(lSQL.Text);
    try
      Result := LerContatos(lConsulta, ABancoDeDados);
    finally
      FreeAndNil(lConsulta);
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

function TRepositorioDeContatosPG.LerContatos(const ADataSet: TDataSet;
  const ABancoDeDados: IBancoDeDados): TObjectList<TContatoPG>;
begin
  Result := TObjectList<TContatoPG>.Create(True);
  try
    while not ADataSet.Eof do
    begin
      Result.Add(LerContato(ADataSet, ABancoDeDados));
      ADataSet.Next;
    end;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TRepositorioDeContatosPG.LerContato(const ADataSet: TDataSet; const ABancoDeDados: IBancoDeDados): TContatoPG;
begin
  Result := TContatoPG.Create;
  try
    Result.IDContato := ADataSet.FieldByName(FIDContato).AsInteger;
    Result.Nome := ADataSet.FieldByName(FNome).AsString;
    Result.Telefones := FRepositorioDeTelefones.CarregarTelefones(Result.IDContato, ABancoDeDados);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

{ TRepositorioDeTelefonesPG }

procedure TRepositorioDeTelefonesPG.InserirTelefones(const ATelefonesPG: TObjectList<TTelefonePG>;
  const ABancoDeDados: IBancoDeDados);
var
  I: Integer;
begin
  ABancoDeDados.IniciarTransacao;
  try
    for I := 0 to Pred(ATelefonesPG.Count) do
      InserirTelefone(ATelefonesPG[I], ABancoDeDados);
    ABancoDeDados.ConfirmarTransacao;
  except
    ABancoDeDados.CancelarTransacao;
    raise;
  end;
end;

procedure TRepositorioDeTelefonesPG.InserirTelefone(const ATelefonePG: TTelefonePG; const ABancoDeDados: IBancoDeDados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('INSERT INTO ' + FTelefone + ' (');
    lSQL.Add('    ' + FIDContato);
    lSQL.Add('  , ' + FDescricao);
    lSQL.Add('  , ' + FNumero);
    lSQL.Add(') VALUES (');
    lSQL.Add('    ' + IntToStr(Integer(ATelefonePG.Contato.IDContato)));
    lSQL.Add('  , ' + QuotedStr(ATelefonePG.Descricao));
    lSQL.Add('  , ' + QuotedStr(ATelefonePG.Numero));
    lSQL.Add(') RETURNING ' + FIDTelefone + ' {INTO :id}');
    ATelefonePG.IDTelefone := ABancoDeDados.Executar(lSQL.Text);
  finally
    FreeAndNil(lSQL);
  end;
end;

procedure TRepositorioDeTelefonesPG.AlterarTelefones(const ATelefonesPG: TObjectList<TTelefonePG>;
  const ABancoDeDados: IBancoDeDados);
var
  lTelefonesPG: TObjectList<TTelefonePG>;
  lIDs: TDictionary<Integer, TTelefonePG>;
  I: Integer;
begin
  lIDs := TDictionary<Integer, TTelefonePG>.Create;
  try
    for I := 0 to Pred(ATelefonesPG.Count) do
      if ATelefonesPG[I].IDTelefone > 0 then
        lIDs.Add(ATelefonesPG[I].IDTelefone, ATelefonesPG[I]);
    lTelefonesPG := CarregarTelefones(ATelefonesPG[0].Contato.IDContato, ABancoDeDados);
    try
      ABancoDeDados.IniciarTransacao;
      try
        for I := 0 to Pred(lTelefonesPG.Count) do
          if not lIDs.ContainsKey(lTelefonesPG[I].IDTelefone) then
            ExcluirTelefone(lTelefonesPG[I], ABancoDeDados);
        for I := 0 to Pred(ATelefonesPG.Count) do
          if ATelefonesPG[I].IDTelefone = 0 then
            InserirTelefone(ATelefonesPG[I], ABancoDeDados)
          else
            AlterarTelefone(ATelefonesPG[I], ABancoDeDados);
        ABancoDeDados.ConfirmarTransacao;
      except
        ABancoDeDados.CancelarTransacao;
        raise;
      end;
    finally
      FreeAndNil(lTelefonesPG);
    end;
  finally
    FreeAndNil(lIDs);
  end;
end;

procedure TRepositorioDeTelefonesPG.AlterarTelefone(const ATelefonePG: TTelefonePG; const ABancoDeDados: IBancoDeDados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('UPDATE ' + FTelefone);
    lSQL.Add('   SET ' + FIDContato + ' = ' + IntToStr(ATelefonePG.Contato.IDContato));
    lSQL.Add('     , ' + FDescricao + ' = ' + QuotedStr(ATelefonePG.Numero));
    lSQL.Add('     , ' + FNumero + ' = ' + QuotedStr(ATelefonePG.Numero));
    lSQL.Add(' WHERE ' + FIDTelefone + ' = ' + IntToStr(ATelefonePG.IDTelefone));
    ABancoDeDados.Executar(lSQL.Text);
  finally
    FreeAndNil(lSQL);
  end;
end;

procedure TRepositorioDeTelefonesPG.ExcluirTelefones(const ATelefonesPG: TObjectList<TTelefonePG>;
  const ABancoDeDados: IBancoDedados);
var
  I: Integer;
begin
  ABancoDeDados.IniciarTransacao;
  try
    for I := 0 to Pred(ATelefonesPG.Count) do
      ExcluirTelefone(ATelefonesPG[I], ABancoDeDados);
    ABancoDeDados.ConfirmarTransacao;
  except
    ABancoDeDados.CancelarTransacao;
    raise;
  end;
end;

procedure TRepositorioDeTelefonesPG.ExcluirTelefone(const ATelefonePG: TTelefonePG; const ABancoDeDados: IBancoDedados);
var
  lSQL: TStrings;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('DELETE FROM ' + FTelefone);
    lSQL.Add('      WHERE ' + FIDTelefone + ' = ' + IntToStr(ATelefonePG.IDTelefone));
    ABancoDeDados.Executar(lSQL.Text);
  finally
    FreeAndNil(lSQL);
  end;
end;

function TRepositorioDeTelefonesPG.CarregarTelefones(const AIDContato: Integer;
  const ABancoDeDados: IBancoDeDados): TObjectList<TTelefonePG>;
var
  lSQL: TStrings;
  lConsulta: TDataSet;
begin
  lSQL := TStringList.Create;
  try
    lSQL.Add('SELECT ' + FIDTelefone);
    lSQL.Add('     , ' + FIDContato);
    lSQL.Add('     , ' + FDescricao);
    lSQL.Add('     , ' + FNumero);
    lSQL.Add('  FROM ' + FTelefone);
    lSQL.Add(' WHERE ' + FIDContato + ' = ' + IntToStr(AIDContato));
    lConsulta := ABancoDeDados.Consultar(lSQL.Text);
    try
      Result := LerTelefones(lConsulta);
    finally
      FreeAndNil(lConsulta);
    end;
  finally
    FreeAndNil(lSQL);
  end;
end;

function TRepositorioDeTelefonesPG.LerTelefones(const ADataSet: TDataSet): TObjectList<TTelefonePG>;
begin
  Result := TObjectList<TTelefonePG>.Create(True);
  try
    while not ADataSet.Eof do
    begin
      Result.Add(LerTelefone(ADataSet));
      ADataSet.Next;
    end;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TRepositorioDeTelefonesPG.LerTelefone(const ADataSet: TDataSet): TTelefonePG;
begin
  Result := TTelefonePG.Create;
  try
    Result.IDTelefone := ADataSet.FieldByName(FIDTelefone).AsInteger;
    Result.Descricao := ADataSet.FieldByName(FDescricao).AsString;
    Result.Numero := ADataSet.FieldByName(FNumero).AsString;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

end.
