unit Adaptadores.Entrada.REST.Controladores.Cliente.ControladorDeExclusaoDeCliente;

interface

uses
  MVCFramework,
  MVCFramework.Commons,
  Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeExclusaoDeCliente;

type
  //TControladorDeExclusaoDeCliente
  [MVCPath('/cliente')]
  TControladorDeExclusaoDeCliente = class(TMVCController)
  private
    FCasoDeUsoDeExclusaoDeCliente: ICasoDeUsoDeExclusaoDeCliente;
  public
    [MVCInject]
    constructor Create(const ACasoDeUsoDeExclusaoDeCliente: ICasoDeUsoDeExclusaoDeCliente); reintroduce;
    [MVCPath('/($ACPFJ)')]
    [MVCHTTPMethod([httpDELETE])]
    [MVCConsumes(TMVCMediaType.APPLICATION_JSON)]
    function Excluir(const ACPFJ: string): IMVCResponse;
  end;

implementation

uses
  System.SysUtils,
  Modelo.Entidades.Cliente,
  Modelo.Valores.CPFJ,
  Aplicacao.Portas.Entrada.Cliente.Erros,
  Adaptadores.Entrada.REST.Controladores.Cliente.ClienteWeb,
  Adaptadores.Entrada.REST.Conversores.ConversorDeCPFJ,
  Adaptadores.Entrada.REST.Resposta.ErroWeb,
  Adaptadores.Entrada.REST.Resposta.GeradorDeRespostaWeb;

{ TControladorDeExclusaoDeCliente }

constructor TControladorDeExclusaoDeCliente.Create(const ACasoDeUsoDeExclusaoDeCliente: ICasoDeUsoDeExclusaoDeCliente);
begin
  inherited Create;
  FCasoDeUsoDeExclusaoDeCliente := ACasoDeUsoDeExclusaoDeCliente;
end;

function TControladorDeExclusaoDeCliente.Excluir(const ACPFJ: string): IMVCResponse;
var
  lCPFJ: TCPFJ;
  lCliente: TCliente;
begin
  try
    lCPFJ := TConversorDeCPFJ.De(ACPFJ);
    try
      lCliente := FCasoDeUsoDeExclusaoDeCliente.Excluir(lCPFJ);
      try
        Result := TGeradorDeRespostaWeb.GerarResposta(HTTP_STATUS.OK, TClienteWeb.ConverterParaWeb(lCliente));
      finally
        FreeAndNil(lCliente);
      end;
    finally
      FreeAndNil(lCPFJ);
    end;
  except
    on E: EClienteNaoEncontrado do
      Result := TGeradorDeRespostaWeb.GerarResposta(HTTP_STATUS.NotFound, E.Message);
    on E: EErroWeb do
      Result := E.Resposta
    else
      raise;
  end;
end;

end.
