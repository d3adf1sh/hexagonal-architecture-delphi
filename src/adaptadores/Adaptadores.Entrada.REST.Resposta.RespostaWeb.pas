unit Adaptadores.Entrada.REST.Resposta.RespostaWeb;

interface

type
  //TRespostaWeb
  TRespostaWeb = class sealed
  private
    FCodigo: Integer;
    FMensagem: string;
  public
    constructor Create(const ACodigo: Integer; const AMensagem: string);
    property Codigo: Integer read FCodigo;
    property Mensagem: string read FMensagem;
  end;

implementation

{ TRespostaWeb }

constructor TRespostaWeb.Create(const ACodigo: Integer; const AMensagem: string);
begin
  FCodigo := ACodigo;
  FMensagem := AMensagem;
end;

end.
