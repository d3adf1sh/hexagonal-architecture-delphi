unit Adaptadores.Entrada.REST.Controladores.Cliente.ControladorDeAlteracaoDeCliente;

interface

uses
  MVCFramework,
  MVCFramework.Commons,
  Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeAlteracaoDeCliente,
  Adaptadores.Entrada.REST.Controladores.Cliente.ClienteWeb;

type
  //TControladorDeAlteracaoDeCliente
  [MVCPath('/cliente')]
  TControladorDeAlteracaoDeCliente = class(TMVCController)
  private
    FCasoDeUsoDeAlteracaoDeCliente: ICasoDeUsoDeAlteracaoDeCliente;
  public
    [MVCInject]
    constructor Create(const ACasoDeUsoDeAlteracaoDeCliente: ICasoDeUsoDeAlteracaoDeCliente); reintroduce;
    [MVCPath('')]
    [MVCHTTPMethod([httpPUT])]
    [MVCConsumes(TMVCMediaType.APPLICATION_JSON)]
    function Alterar(const [MVCFromBody] AClienteWeb: TClienteWeb): IMVCResponse;
  end;

implementation

uses
  System.SysUtils,
  Modelo.Entidades.Cliente,
  Aplicacao.Portas.Entrada.Cliente.Erros,
  Adaptadores.Entrada.REST.Resposta.ErroWeb,
  Adaptadores.Entrada.REST.Resposta.GeradorDeRespostaWeb;

{ TControladorDeAlteracaoDeCliente }

constructor TControladorDeAlteracaoDeCliente.Create(
  const ACasoDeUsoDeAlteracaoDeCliente: ICasoDeUsoDeAlteracaoDeCliente);
begin
  inherited Create;
  FCasoDeUsoDeAlteracaoDeCliente := ACasoDeUsoDeAlteracaoDeCliente;
end;

function TControladorDeAlteracaoDeCliente.Alterar(const AClienteWeb: TClienteWeb): IMVCResponse;
var
  lCliente: TCliente;
  lClienteDeResposta: TCliente;
begin
  try
    lCliente := TClienteWeb.ConverterParaModelo(AClienteWeb);
    try
      lClienteDeResposta := FCasoDeUsoDeAlteracaoDeCliente.Alterar(lCliente);
      try
        Result := TGeradorDeRespostaWeb.GerarResposta(HTTP_STATUS.OK, TClienteWeb.ConverterParaWeb(lClienteDeResposta));
      finally
        FreeAndNil(lClienteDeResposta);
      end;
    finally
      FreeAndNil(lCliente);
    end;
  except
    on E: EArgumentException do
      Result := TGeradorDeRespostaWeb.GerarResposta(HTTP_STATUS.BadRequest, E.Message);
    on E: EClienteNaoEncontrado do
      Result := TGeradorDeRespostaWeb.GerarResposta(HTTP_STATUS.NotFound, E.Message);
    on E: EErroWeb do
      Result := E.Resposta
    else
      raise;
  end;
end;

end.
