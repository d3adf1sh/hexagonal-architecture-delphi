unit Adaptadores.Entrada.REST.Conversores.ConversorDeCPFJ;

interface

uses
  Modelo.Valores.CPFJ;

type
  //TConversorDeCPFJ
  TConversorDeCPFJ = class sealed
  public
    class function De(const ACPFJ: string): TCPFJ;
  end;

implementation

uses
  System.SysUtils,
  MVCFramework.Commons,
  Adaptadores.Entrada.REST.Resposta.GeradorDeRespostaWeb;

{ TConversorDeCPFJ }

class function TConversorDeCPFJ.De(const ACPFJ: string): TCPFJ;
begin
  try
    Result := TCPFJ.De(ACPFJ);
  except
    on E: EArgumentException do
      raise TGeradorDeRespostaWeb.GerarErro(HTTP_STATUS.BadRequest, 'CNPJ/CPF inv�lido.');
    else
      raise;
  end;
end;

end.
