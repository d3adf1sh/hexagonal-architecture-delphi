unit Adaptadores.Entrada.REST.Controladores.Cliente.ControladorDeConsultaDeClientes;

interface

uses
  MVCFramework,
  MVCFramework.Commons,
  Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeConsultaDeClientes;

type
  //TControladorDeConsultaDeClientes
  [MVCPath('/cliente')]
  TControladorDeConsultaDeClientes = class(TMVCController)
  private
    FCasoDeUsoDeConsultaDeClientes: ICasoDeUsoDeConsultaDeClientes;
  public
    [MVCInject]
    constructor Create(const ACasoDeUsoDeConsultaDeClientes: ICasoDeUsoDeConsultaDeClientes); reintroduce;
    [MVCPath('')]
    [MVCHTTPMethod([httpGET])]
    [MVCConsumes(TMVCMediaType.APPLICATION_JSON)]
    function Consultar(const [MVCFromQueryString('nomeFantasia', '')] ANomeFantasia: string): IMVCResponse;
  end;

implementation

uses
  System.SysUtils,
  System.Generics.Collections,
  Modelo.Entidades.Cliente,
  Adaptadores.Entrada.REST.Controladores.Cliente.ClienteWeb,
  Adaptadores.Entrada.REST.Resposta.ErroWeb,
  Adaptadores.Entrada.REST.Resposta.GeradorDeRespostaWeb;

{ TControladorDeConsultaDeClientes }

constructor TControladorDeConsultaDeClientes.Create(
  const ACasoDeUsoDeConsultaDeClientes: ICasoDeUsoDeConsultaDeClientes);
begin
  inherited Create;
  FCasoDeUsoDeConsultaDeClientes := ACasoDeUsoDeConsultaDeClientes;
end;

function TControladorDeConsultaDeClientes.Consultar(const ANomeFantasia: string): IMVCResponse;
var
  lClientes: TObjectList<TCliente>;
begin
  try
    lClientes := FCasoDeUsoDeConsultaDeClientes.Consultar(ANomeFantasia);
    try
      Result := TGeradorDeRespostaWeb.GerarResposta(HTTP_STATUS.OK, TClienteWeb.ConverterParaWeb(lClientes));
    finally
      FreeAndNil(lClientes);
    end;
  except
    on E: EArgumentException do
      Result := TGeradorDeRespostaWeb.GerarResposta(HTTP_STATUS.BadRequest, E.Message);
    on E: EErroWeb do
      Result := E.Resposta
    else
      raise;
  end;
end;

end.
