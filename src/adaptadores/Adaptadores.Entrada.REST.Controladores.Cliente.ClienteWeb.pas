unit Adaptadores.Entrada.REST.Controladores.Cliente.ClienteWeb;

interface

uses
  System.Generics.Collections,
  Modelo.Entidades.Cliente;

type
  //Forward declarations
  TContatoWeb = class;
  TTelefoneWeb = class;

  //TClienteWeb
  TClienteWeb = class sealed
  private
    FNomeFantasia: string;
    FRazaoSocial: string;
    FCPFJ: string;
    FContatos: TObjectList<TContatoWeb>;
    procedure SetContatos(const AValor: TObjectList<TContatoWeb>);
  public
    constructor Create;
    destructor Destroy; override;
    property NomeFantasia: string read FNomeFantasia write FNomeFantasia;
    property RazaoSocial: string read FRazaoSocial write FRazaoSocial;
    property CPFJ: string read FCPFJ write FCPFJ;
    property Contatos: TObjectList<TContatoWeb> read FContatos write SetContatos;
    class function ConverterParaWeb(const AClientes: TObjectList<TCliente>): TObjectList<TClienteWeb>; overload;
    class function ConverterParaWeb(const ACliente: TCliente): TClienteWeb; overload;
    class function ConverterParaModelo(const AClientesWeb: TObjectList<TClienteWeb>): TObjectList<TCliente>; overload;
    class function ConverterParaModelo(const AClienteWeb: TClienteWeb): TCliente; overload;
  end;

  //TContatoWeb
  TContatoWeb = class sealed
  private
    FNome: string;
    FTelefones: TObjectList<TTelefoneWeb>;
    procedure SetTelefones(const AValor: TObjectList<TTelefoneWeb>);
  public
    constructor Create;
    destructor Destroy; override;
    property Nome: string read FNome write FNome;
    property Telefones: TObjectList<TTelefoneWeb> read FTelefones write SetTelefones;
    class function ConverterParaWeb(const AContatos: TObjectList<TContato>): TObjectList<TContatoWeb>; overload;
    class function ConverterParaWeb(const AContato: TContato): TContatoWeb; overload;
    class function ConverterParaModelo(const AContatosWeb: TObjectList<TContatoWeb>): TObjectList<TContato>; overload;
    class function ConverterParaModelo(const AContatoWeb: TContatoWeb): TContato; overload;
  end;

  //TTelefoneWeb
  TTelefoneWeb = class sealed
  private
    FDescricao: string;
    FNumero: string;
  public
    property Descricao: string read FDescricao write FDescricao;
    property Numero: string read FNumero write FNumero;
    class function ConverterParaWeb(const ATelefones: TObjectList<TTelefone>): TObjectList<TTelefoneWeb>; overload;
    class function ConverterParaWeb(const ATelefone: TTelefone): TTelefoneWeb; overload;
    class function ConverterParaModelo(const ATelefonesWeb: TObjectList<TTelefoneWeb>): TObjectList<TTelefone>; overload;
    class function ConverterParaModelo(const ATelefoneWeb: TTelefoneWeb): TTelefone; overload;
  end;

implementation

uses
  System.SysUtils,
  Modelo.Valores.CPFJ,
  Modelo.Valores.Numero,
  Adaptadores.Entrada.REST.Conversores.ConversorDeCPFJ,
  Adaptadores.Entrada.REST.Conversores.ConversorDeNumero;

{ TClienteWeb }

constructor TClienteWeb.Create;
begin
  FContatos := TObjectList<TContatoWeb>.Create(True);
end;

destructor TClienteWeb.Destroy;
begin
  FreeAndNil(FContatos);
  inherited;
end;

procedure TClienteWeb.SetContatos(const AValor: TObjectList<TContatoWeb>);
begin
  if AValor <> FContatos then
  begin
    if Assigned(FContatos) then
      FreeAndNil(FContatos);
    FContatos := AValor;
  end;
end;

class function TClienteWeb.ConverterParaWeb(const AClientes: TObjectList<TCliente>): TObjectList<TClienteWeb>;
var
  I: Integer;
begin
  Result := TObjectList<TClienteWeb>.Create(True);
  try
    for I := 0 to Pred(AClientes.Count) do
      Result.Add(ConverterParaWeb(AClientes[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TClienteWeb.ConverterParaWeb(const ACliente: TCliente): TClienteWeb;
begin
  Result := TClienteWeb.Create;
  try
    Result.NomeFantasia := ACliente.NomeFantasia;
    Result.RazaoSocial := ACliente.RazaoSocial;
    if Assigned(ACliente.CPFJ) then
      Result.CPFJ := ACliente.CPFJ.Formatar
    else
      Result.CPFJ := '';
    if Assigned(ACliente.Contatos) then
      Result.Contatos := TContatoWeb.ConverterParaWeb(ACliente.Contatos)
    else
      Result.Contatos := nil;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TClienteWeb.ConverterParaModelo(const AClientesWeb: TObjectList<TClienteWeb>): TObjectList<TCliente>;
var
  I: Integer;
begin
  Result := TObjectList<TCliente>.Create(True);
  try
    for I := 0 to Pred(AClientesWeb.Count) do
      Result.Add(ConverterParaModelo(AClientesWeb[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TClienteWeb.ConverterParaModelo(const AClienteWeb: TClienteWeb): TCliente;
var
  lCPFJ: TCPFJ;
  lContatos: TObjectList<TContato>;
begin
  lCPFJ := nil;
  lContatos := nil;
  try
    if AClienteWeb.CPFJ <> '' then
      lCPFJ := TConversorDeCPFJ.De(AClienteWeb.CPFJ);
    if Assigned(AClienteWeb.Contatos) then
      lContatos := TContatoWeb.ConverterParaModelo(AClienteWeb.Contatos);
    Result := TCliente.Create(AClienteWeb.NomeFantasia, AClienteWeb.RazaoSocial, lCPFJ, lContatos);
  except
    if Assigned(lCPFJ) then
      FreeAndNil(lCPFJ);
    if Assigned(lContatos) then
      FreeAndNil(lContatos);
    raise;
  end;
end;

{ TContatoWeb }

constructor TContatoWeb.Create;
begin
  FTelefones := TObjectList<TTelefoneWeb>.Create(True);
end;

destructor TContatoWeb.Destroy;
begin
  FreeAndNil(FTelefones);
  inherited;
end;

procedure TContatoWeb.SetTelefones(const AValor: TObjectList<TTelefoneWeb>);
begin
  if AValor <> FTelefones then
  begin
    if Assigned(FTelefones) then
      FreeAndNil(FTelefones);
    FTelefones := AValor;
  end;
end;

class function TContatoWeb.ConverterParaWeb(const AContatos: TObjectList<TContato>): TObjectList<TContatoWeb>;
var
  I: Integer;
begin
  Result := TObjectList<TContatoWeb>.Create(True);
  try
    for I := 0 to Pred(AContatos.Count) do
      Result.Add(ConverterParaWeb(AContatos[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TContatoWeb.ConverterParaWeb(const AContato: TContato): TContatoWeb;
begin
  Result := TContatoWeb.Create;
  try
    Result.Nome := AContato.Nome;
    if Assigned(AContato.Telefones) then
      Result.Telefones := TTelefoneWeb.ConverterParaWeb(AContato.Telefones)
    else
      Result.Telefones := nil;
  except
    FreeAndNil(Result);
    raise;
  end;
end;


class function TContatoWeb.ConverterParaModelo(const AContatosWeb: TObjectList<TContatoWeb>): TObjectList<TContato>;
var
  I: Integer;
begin
  Result := TObjectList<TContato>.Create(True);
  try
    for I := 0 to Pred(AContatosWeb.Count) do
      Result.Add(ConverterParaModelo(AContatosWeb[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TContatoWeb.ConverterParaModelo(const AContatoWeb: TContatoWeb): TContato;
var
  lTelefones: TObjectList<TTelefone>;
begin
  lTelefones := nil;
  try
    if Assigned(AContatoWeb.Telefones) then
      lTelefones := TTelefoneWeb.ConverterParaModelo(AContatoWeb.Telefones);
    Result := TContato.Create(AContatoWeb.Nome, lTelefones);
  except
    if Assigned(lTelefones) then
      FreeAndNil(lTelefones);
    raise;
  end;
end;

{ TTelefoneWeb }

class function TTelefoneWeb.ConverterParaWeb(const ATelefones: TObjectList<TTelefone>): TObjectList<TTelefoneWeb>;
var
  I: Integer;
begin
  Result := TObjectList<TTelefoneWeb>.Create(True);
  try
    for I := 0 to Pred(ATelefones.Count) do
      Result.Add(ConverterParaWeb(ATelefones[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TTelefoneWeb.ConverterParaWeb(const ATelefone: TTelefone): TTelefoneWeb;
begin
  Result := TTelefoneWeb.Create;
  try
    Result.Descricao := ATelefone.Descricao;
    if Assigned(ATelefone.Numero) then
      Result.Numero := ATelefone.Numero.Formatar
    else
      Result.Numero := '';
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TTelefoneWeb.ConverterParaModelo(const ATelefonesWeb: TObjectList<TTelefoneWeb>): TObjectList<TTelefone>;
var
  I: Integer;
begin
  Result := TObjectList<TTelefone>.Create(True);
  try
    for I := 0 to Pred(ATelefonesWeb.Count) do
      Result.Add(ConverterParaModelo(ATelefonesWeb[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TTelefoneWeb.ConverterParaModelo(const ATelefoneWeb: TTelefoneWeb): TTelefone;
var
  lNumero: TNumero;
begin
  lNumero := nil;
  try
    if ATelefoneWeb.Numero <> '' then
      lNumero := TConversorDeNumero.De(ATelefoneWeb.Numero);
    Result := TTelefone.Create(ATelefoneWeb.Descricao, lNumero);
  except
    if Assigned(lNumero) then
      FreeAndNil(lNumero);
    raise;
  end;
end;

end.

