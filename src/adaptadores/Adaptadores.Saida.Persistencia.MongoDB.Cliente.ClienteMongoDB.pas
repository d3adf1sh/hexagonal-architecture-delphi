unit Adaptadores.Saida.Persistencia.MongoDB.Cliente.ClienteMongoDB;

interface

uses
  System.Generics.Collections,
  System.JSON,
  FireDAC.Phys.MongoDBWrapper,
  Modelo.Entidades.Cliente;

type
  //TClienteMongoDB
  TClienteMongoDB = class sealed
  public
    class function ConverterParaJSON(const AClientes: TObjectList<TCliente>): TJSONArray; overload;
    class function ConverterParaJSON(const ACliente: TCliente): TJSONObject; overload;
    class function ConverterParaModelo(const AJSONDeClientes: TJSONArray): TObjectList<TCliente>; overload;
    class function ConverterParaModelo(const AJSONDeCliente: TJSONObject): TCliente; overload;
  public const
    Cliente = 'cliente';
    NomeFantasia = 'nomeFantasia';
    RazaoSocial = 'razaoSocial';
    CPFJ = 'cpfj';
    Contatos = 'contatos';
    DataDeInsercao = 'dataDeInsercao';
    DataDeAlteracao = 'dataDeAlteracao';
  end;

  //TContatoMongoDB
  TContatoMongoDB = class sealed
  public
    class function ConverterParaJSON(const AContatos: TObjectList<TContato>): TJSONArray; overload;
    class function ConverterParaJSON(const AContato: TContato): TJSONObject; overload;
    class function ConverterParaModelo(const AJSONDeContatos: TJSONArray): TObjectList<TContato>; overload;
    class function ConverterParaModelo(const AJSONDeContato: TJSONObject): TContato; overload;
  public const
    Nome = 'nome';
    Telefones = 'telefones';
  end;

  //TTelefoneMongoDB
  TTelefoneMongoDB = class sealed
  public
    class function ConverterParaJSON(const ATelefones: TObjectList<TTelefone>): TJSONArray; overload;
    class function ConverterParaJSON(const ATelefone: TTelefone): TJSONObject; overload;
    class function ConverterParaModelo(const AJSONDeTelefones: TJSONArray): TObjectList<TTelefone>; overload;
    class function ConverterParaModelo(const AJSONDeTelefone: TJSONObject): TTelefone; overload;
  public const
    Descricao = 'descricao';
    Numero = 'numero';
  end;

implementation

uses
  System.SysUtils,
  Modelo.Valores.CPFJ,
  Modelo.Valores.Numero;

{ TClienteMongoDB }

class function TClienteMongoDB.ConverterParaJSON(const AClientes: TObjectList<TCliente>): TJSONArray;
var
  I: Integer;
begin
  Result := TJSONArray.Create;
  try
    for I := 0 to Pred(AClientes.Count) do
      Result.Add(ConverterParaJSON(AClientes[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TClienteMongoDB.ConverterParaJSON(const ACliente: TCliente): TJSONObject;
begin
  Result := TJSONObject.Create;
  try
    Result.AddPair(NomeFantasia, ACliente.NomeFantasia);
    Result.AddPair(RazaoSocial, ACliente.RazaoSocial);
    if Assigned(ACliente.CPFJ) then
      Result.AddPair(CPFJ, ACliente.CPFJ.Valor);
    if Assigned(ACliente.Contatos) then
      Result.AddPair(Contatos, TContatoMongoDB.ConverterParaJSON(ACliente.Contatos));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TClienteMongoDB.ConverterParaModelo(const AJSONDeClientes: TJSONArray): TObjectList<TCliente>;
var
  I: Integer;
begin
  Result := TObjectList<TCliente>.Create(True);
  try
    for I := 0 to Pred(AJSONDeClientes.Count) do
      Result.Add(ConverterParaModelo(AJSONDeClientes[I] as TJSONObject));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TClienteMongoDB.ConverterParaModelo(const AJSONDeCliente: TJSONObject): TCliente;
var
  sNomeFantasia: string;
  sRazaoSocial: string;
  lCPFJ: TCPFJ;
  lContatos: TObjectList<TContato>;
  lValor: TJSONValue;
begin
  sNomeFantasia := '';
  sRazaoSocial := '';
  lCPFJ := nil;
  lContatos := nil;
  try
    lValor := AJSONDeCliente.GetValue(NomeFantasia);
    if Assigned(lValor) then
      sNomeFantasia := lValor.Value;
    lValor := AJSONDeCliente.GetValue(RazaoSocial);
    if Assigned(lValor) then
      sRazaoSocial := lValor.Value;
    lValor := AJSONDeCliente.GetValue(CPFJ);
    if Assigned(lValor) then
      lCPFJ := TCPFJ.De(lValor.Value);
    lValor := AJSONDeCliente.GetValue(Contatos);
    if lValor is TJSONArray then
      lContatos := TContatoMongoDB.ConverterParaModelo(TJSONArray(lValor));
    Result := TCliente.Create(sNomeFantasia, sRazaoSocial, lCPFJ, lContatos);
  except
    if Assigned(lCPFJ) then
      FreeAndNil(lCPFJ);
    if Assigned(lContatos) then
      FreeAndNil(lContatos);
    raise;
  end;
end;

{ TContatoMongoDB }

class function TContatoMongoDB.ConverterParaJSON(const AContatos: TObjectList<TContato>): TJSONArray;
var
  I: Integer;
begin
  Result := TJSONArray.Create;
  try
    for I := 0 to Pred(AContatos.Count) do
      Result.Add(ConverterParaJSON(AContatos[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TContatoMongoDB.ConverterParaJSON(const AContato: TContato): TJSONObject;
begin
  Result := TJSONObject.Create;
  try
    Result.AddPair(Nome, AContato.Nome);
    if Assigned(AContato.Telefones) then
      Result.AddPair(Telefones, TTelefoneMongoDB.ConverterParaJSON(AContato.Telefones));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TContatoMongoDB.ConverterParaModelo(const AJSONDeContatos: TJSONArray): TObjectList<TContato>;
var
  I: Integer;
begin
  Result := TObjectList<TContato>.Create(True);
  try
    for I := 0 to Pred(AJSONDeContatos.Count) do
      if AJSONDeContatos[I] is TJSONObject then
        Result.Add(ConverterParaModelo(TJSONObject(AJSONDeContatos[I])));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TContatoMongoDB.ConverterParaModelo(const AJSONDeContato: TJSONObject): TContato;
var
  sNome: string;
  lTelefones: TObjectList<TTelefone>;
  lValor: TJSONValue;
begin
  sNome := '';
  lTelefones := nil;
  try
    lValor := AJSONDeContato.GetValue(Nome);
    if Assigned(lValor) then
      sNome := lValor.Value;
    lValor := AJSONDeContato.GetValue(Telefones);
    if lValor is TJSONArray then
      lTelefones := TTelefoneMongoDB.ConverterParaModelo(TJSONArray(lValor));
    Result := TContato.Create(sNome, lTelefones);
  except
    if Assigned(lTelefones) then
      FreeAndNil(lTelefones);
    raise;
  end;
end;

{ TTelefoneMongoDB }

class function TTelefoneMongoDB.ConverterParaJSON(const ATelefones: TObjectList<TTelefone>): TJSONArray;
var
  I: Integer;
begin
  Result := TJSONArray.Create;
  try
    for I := 0 to Pred(ATelefones.Count) do
      Result.Add(ConverterParaJSON(ATelefones[I]));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TTelefoneMongoDB.ConverterParaJSON(const ATelefone: TTelefone): TJSONObject;
begin
  Result := TJSONObject.Create;
  try
    Result.AddPair(Descricao, ATelefone.Descricao);
    if Assigned(ATelefone.Numero) then
      Result.AddPair(Numero, ATelefone.Numero.Valor);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TTelefoneMongoDB.ConverterParaModelo(const AJSONDeTelefones: TJSONArray): TObjectList<TTelefone>;
var
  I: Integer;
begin
  Result := TObjectList<TTelefone>.Create(True);
  try
    for I := 0 to Pred(AJSONDeTelefones.Count) do
      if AJSONDeTelefones[I] is TJSONObject then
        Result.Add(ConverterParaModelo(TJSONObject(AJSONDeTelefones[I])));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function TTelefoneMongoDB.ConverterParaModelo(const AJSONDeTelefone: TJSONObject): TTelefone;
var
  sDescricao: string;
  lNumero: TNumero;
  lValor: TJSONValue;
begin
  sDescricao := '';
  lNumero := nil;
  try
    lValor := AJSONDeTelefone.GetValue(Descricao);
    if Assigned(lValor) then
      sDescricao := lValor.Value;
    lValor := AJSONDeTelefone.GetValue(Numero);
    if Assigned(lValor) then
      lNumero := TNumero.Create(lValor.Value);
    Result := TTelefone.Create(sDescricao, lNumero);
  except
    if Assigned(lNumero) then
      FreeAndNil(lNumero);
    raise;
  end;
end;

end.

