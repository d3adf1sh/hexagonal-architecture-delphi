unit Adaptadores.Entrada.REST.Resposta.GeradorDeRespostaWeb;

interface

uses
  System.SysUtils,
  MVCFramework;

type
  //GeradorDeRespostaWeb
  TGeradorDeRespostaWeb = class sealed
  public
    class function GerarErro(const AStatus: Integer; const AMensagem: string): Exception;
    class function GerarResposta(const AStatus: Integer; const AMensagem: string): IMVCResponse; overload;
    class function GerarResposta(const AStatus: Integer; const AEntidade: TObject): IMVCResponse; overload;
  end;

implementation

uses
  Adaptadores.Entrada.REST.Resposta.RespostaWeb,
  Adaptadores.Entrada.REST.Resposta.ErroWeb;

{ TGeradorDeRespostaWeb }

class function TGeradorDeRespostaWeb.GerarErro(const AStatus: Integer; const AMensagem: string): Exception;
begin
  Result := EErroWeb.Create(GerarResposta(AStatus, AMensagem));
end;

class function TGeradorDeRespostaWeb.GerarResposta(const AStatus: Integer; const AMensagem: string): IMVCResponse;
begin
  Result := GerarResposta(AStatus, TRespostaWeb.Create(AStatus, AMensagem));
end;

class function TGeradorDeRespostaWeb.GerarResposta(const AStatus: Integer; const AEntidade: TObject): IMVCResponse;
begin
  Result := MVCResponseBuilder.StatusCode(AStatus).Body(AEntidade, True).Build;
end;

end.
