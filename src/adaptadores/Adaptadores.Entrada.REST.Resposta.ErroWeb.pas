unit Adaptadores.Entrada.REST.Resposta.ErroWeb;

interface

uses
  System.SysUtils,
  MVCFramework,
  Adaptadores.Entrada.REST.Resposta.RespostaWeb;

type
  //EErroWeb
  EErroWeb = class(Exception)
  private
    FResposta: IMVCResponse;
  public
    constructor Create(const AResposta: IMVCResponse);
    property Resposta: IMVCResponse read FResposta;
  end;

implementation

{ EErroWeb }

constructor EErroWeb.Create(const AResposta: IMVCResponse);
var
  sMensagem: string;
begin
  if AResposta.Data is TRespostaWeb then
    sMensagem := TRespostaWeb(AResposta.Data).Mensagem
  else
    sMensagem := '';
  inherited Create(sMensagem);
  FResposta := AResposta;
end;

end.
