unit Bootstrap.Aplicacao.Erros;

interface

uses
  System.SysUtils;

type
  //EServidorJaIniciado
  EServidorJaIniciado = class(Exception);

  //EServidorNaoIniciado
  EServidorNaoIniciado = class(Exception);

  //EPersistencia
  EPersistenciaInvalida = class(Exception);

implementation

end.
