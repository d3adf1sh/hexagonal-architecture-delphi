program Bootstrap;

uses
  Vcl.Forms,
  Bootstrap.Aplicacao.Erros in 'Bootstrap.Aplicacao.Erros.pas',
  Bootstrap.Aplicacao in 'Bootstrap.Aplicacao.pas',
  Bootstrap.Servidor in 'Bootstrap.Servidor.pas' {Servidor: TWebModule},
  Bootstrap.Tela in 'Bootstrap.Tela.pas' {Tela};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Exemplo de Arquitetura Hexagonal';
  Application.CreateForm(TTela, Tela);
  Application.Run;
end.
