unit Bootstrap.Aplicacao;

interface

uses
  IDHTTPWebBrokerBridge,
  MVCFramework.Container;

type
  //TAplicacao
  TAplicacao = class sealed
  private class var
    FInstancia: TAplicacao;
  private
    FServidor: TIDHTTPWebBrokerBridge;
    procedure RegistrarAdaptadores; overload;
    procedure RegistrarAdaptadores(const AContainer: IMVCServiceContainer); overload;
    procedure RegistrarAdaptadoresDePersistencia(const AContainer: IMVCServiceContainer);
    procedure RegistrarAdaptadoresParaMySQL(const AContainer: IMVCServiceContainer);
    procedure RegistrarAdaptadoresParaPG(const AContainer: IMVCServiceContainer);
    procedure RegistrarAdaptadoresParaMongoDB(const AContainer: IMVCServiceContainer);
    procedure RegistrarServicos(const AContainer: IMVCServiceContainer);
  public
    constructor Create;
    destructor Destroy; override;
    procedure CarregarParametros; overload;
    procedure CarregarParametros(const ANomeDoArquivo: string); overload;
    procedure IniciarServidor;
    function ServidorIniciado: Boolean;
    procedure PararServidor;
    class constructor Create;
    class destructor Destroy;
    class property Instancia: TAplicacao read FInstancia;
  end;

implementation

uses
  System.SysUtils,
  Web.WebReq,
  Infraestrutura.Parametros,
  Infraestrutura.Parametros.Constantes,
  Infraestrutura.Persistencia.FabricaDeBancoDeDados,
  Infraestrutura.Persistencia.FabricaDeBancoDeDadosMySQL,
  Infraestrutura.Persistencia.FabricaDeBancoDeDadosPG,
  Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeInsercaoDeCliente,
  Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeAlteracaoDeCliente,
  Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeExclusaoDeCliente,
  Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeBuscaDeCliente,
  Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeConsultaDeClientes,
  Aplicacao.Portas.Saida.RepositorioDeClientes,
  Aplicacao.Servicos.Cliente.ServicoDeInsercaoDeCliente,
  Aplicacao.Servicos.Cliente.ServicoDeAlteracaoDeCliente,
  Aplicacao.Servicos.Cliente.ServicoDeExclusaoDeCliente,
  Aplicacao.Servicos.Cliente.ServicoDeBuscaDeCliente,
  Aplicacao.Servicos.Cliente.ServicoDeConsultaDeClientes,
  Adaptadores.Saida.Persistencia.MySQL.Cliente.RepositorioDeClientesMySQL,
  Adaptadores.Saida.Persistencia.PG.Cliente.RepositorioDeClientesPG,
  Adaptadores.Saida.Persistencia.MongoDB.Cliente.RepositorioDeClientesMongoDB,
  Bootstrap.Aplicacao.Erros,
  Bootstrap.Servidor;

{ TAplicacao }

constructor TAplicacao.Create;
begin
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := TServidor;
end;

destructor TAplicacao.Destroy;
begin
  if ServidorIniciado then
    PararServidor;
  inherited;
end;

procedure TAplicacao.RegistrarAdaptadores;
begin
  RegistrarAdaptadores(DefaultMVCServiceContainer);
end;

procedure TAplicacao.RegistrarAdaptadores(const AContainer: IMVCServiceContainer);
begin
  RegistrarAdaptadoresDePersistencia(AContainer);
  RegistrarServicos(AContainer);
  AContainer.Build;
end;

procedure TAplicacao.RegistrarAdaptadoresDePersistencia(const AContainer: IMVCServiceContainer);
var
  sPersistencia: string;
begin
  sPersistencia := TParametros.Instancia.Ler(CPersistencia);
  if sPersistencia = CMySQL then
    RegistrarAdaptadoresParaMySQL(AContainer)
  else if sPersistencia = CPG then
    RegistrarAdaptadoresParaPG(AContainer)
  else if sPersistencia = CMongoDB then
    RegistrarAdaptadoresParaMongoDB(AContainer)
  else
    raise EPersistenciaInvalida.CreateFmt('Persist�ncia "%s" inv�lida.', [sPersistencia]);
end;

procedure TAplicacao.RegistrarAdaptadoresParaMySQL(const AContainer: IMVCServiceContainer);
begin
  AContainer.RegisterType(TRepositorioDeClientesMySQL, IRepositorioDeClientes);
  AContainer.RegisterType(TFabricaDeBancoDeDadosMySQL, IFabricaDeBancoDeDados);
end;

procedure TAplicacao.RegistrarAdaptadoresParaPG(const AContainer: IMVCServiceContainer);
begin
  AContainer.RegisterType(TRepositorioDeClientesPG, IRepositorioDeClientes);
  AContainer.RegisterType(TFabricaDeBancoDeDadosPG, IFabricaDeBancoDeDados);
end;

procedure TAplicacao.RegistrarAdaptadoresParaMongoDB(const AContainer: IMVCServiceContainer);
begin
  AContainer.RegisterType(TRepositorioDeClientesMongoDB, IRepositorioDeClientes);
end;

procedure TAplicacao.RegistrarServicos(const AContainer: IMVCServiceContainer);
begin
  AContainer.RegisterType(TServicoDeInsercaoDeCliente, ICasoDeUsoDeInsercaoDeCliente);
  AContainer.RegisterType(TServicoDeAlteracaoDeCliente, ICasoDeUsoDeAlteracaoDeCliente);
  AContainer.RegisterType(TServicoDeExclusaoDeCliente, ICasoDeUsoDeExclusaoDeCliente);
  AContainer.RegisterType(TServicoDeBuscaDeCliente, ICasoDeUsoDeBuscaDeCliente);
  AContainer.RegisterType(TServicoDeConsultaDeClientes, ICasoDeUsoDeConsultaDeClientes);
end;

procedure TAplicacao.CarregarParametros;
begin
  CarregarParametros(IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) + 'config.properties');
end;

procedure TAplicacao.CarregarParametros(const ANomeDoArquivo: string);
begin
  TParametros.Instancia.Carregar(ANomeDoArquivo);
end;

procedure TAplicacao.IniciarServidor;
begin
  if ServidorIniciado then
    raise EServidorJaIniciado.Create('Servidor j� iniciado.');
  try
    RegistrarAdaptadores;
    FServidor := TIDHTTPWebBrokerBridge.Create(nil);
    FServidor.DefaultPort := 8080;
    FServidor.MaxConnections := 1024;
    FServidor.ListenQueue := 200;
    FServidor.Active := True;
  except
    FreeAndNil(FServidor);
    raise;
  end;
end;

function TAplicacao.ServidorIniciado: Boolean;
begin
  Result := Assigned(FServidor) and FServidor.Active;
end;

procedure TAplicacao.PararServidor;
begin
  if not ServidorIniciado then
    raise EServidorNaoIniciado.Create('Servidor n�o iniciado.');
  try
    FServidor.Active := False;
  finally
    FreeAndNil(FServidor);
  end;
end;

class constructor TAplicacao.Create;
begin
  FInstancia := TAplicacao.Create;
end;

class destructor TAplicacao.Destroy;
begin
  FreeAndNil(FInstancia);
end;

end.
