unit Bootstrap.Servidor;

interface

uses
  Web.HTTPApp,
  MVCFramework;

type
  //TServidor
  TServidor = class(TWebModule)
    procedure WebModuleCreate(Sender: TObject);
    procedure WebModuleDestroy(Sender: TObject);
  private
    FMVC: TMVCEngine;
  end;

implementation

{$R *.dfm}

uses
  System.SysUtils,
  Adaptadores.Entrada.REST.Controladores.Cliente.ControladorDeInsercaoDeCliente,
  Adaptadores.Entrada.REST.Controladores.Cliente.ControladorDeAlteracaoDeCliente,
  Adaptadores.Entrada.REST.Controladores.Cliente.ControladorDeExclusaoDeCliente,
  Adaptadores.Entrada.REST.Controladores.Cliente.ControladorDeBuscaDeCliente,
  Adaptadores.Entrada.REST.Controladores.Cliente.ControladorDeConsultaDeClientes;

{ TWebServer }

procedure TServidor.WebModuleCreate(Sender: TObject);
begin
  FMVC := TMVCEngine.Create(Self);
  FMVC.AddController(TControladorDeInsercaoDeCliente);
  FMVC.AddController(TControladorDeAlteracaoDeCliente);
  FMVC.AddController(TControladorDeExclusaoDeCliente);
  FMVC.AddController(TControladorDeBuscaDeCliente);
  FMVC.AddController(TControladorDeConsultaDeClientes);
end;

procedure TServidor.WebModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FMVC);
end;

end.
