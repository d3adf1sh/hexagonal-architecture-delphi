unit Bootstrap.Tela;

interface

uses
  System.Classes,
  VCL.Controls,
  VCL.Forms,
  VCL.StdCtrls;

type
  //TfrmPrincipal
  TTela = class(TForm)
    mMensagens: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  end;

var
  Tela: TTela;

implementation

uses
  System.SysUtils,
  Bootstrap.Aplicacao;

{$R *.dfm}

procedure TTela.FormCreate(Sender: TObject);
begin
  ReportMemoryLeaksOnShutdown := True;
  Caption := Application.Title;
  try
    mMensagens.Lines.Add('Carregando parāmetros...');
    TAplicacao.Instancia.CarregarParametros;
    mMensagens.Lines.Add('Iniciando servidor...');
    TAplicacao.Instancia.IniciarServidor;
    mMensagens.Lines.Add('Servidor iniciado.');
  except
    on E: Exception do
      mMensagens.Lines.Add('Erro: ' + E.Message);
  end;
end;

procedure TTela.FormDestroy(Sender: TObject);
begin
  if TAplicacao.Instancia.ServidorIniciado then
    TAplicacao.Instancia.PararServidor;
end;

end.
