unit Aplicacao.Servicos.Cliente.ServicoDeBuscaDeCliente;

interface

uses
  System.SysUtils,
  Modelo.Entidades.Cliente,
  Modelo.Valores.CPFJ,
  Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeBuscaDeCliente,
  Aplicacao.Portas.Saida.RepositorioDeClientes;

type
  //TServicoDeBuscaDeCliente
  TServicoDeBuscaDeCliente = class sealed(TInterfacedObject, ICasoDeUsoDeBuscaDeCliente)
  private
    FRepositorioDeClientes: IRepositorioDeClientes;
  public
    constructor Create(const ARepositorioDeClientes: IRepositorioDeClientes);
    function Buscar(const ACPFJ: TCPFJ): TCliente;
  end;

implementation

uses
  Aplicacao.Portas.Entrada.Cliente.Erros;

{ TServicoDeBuscaDeCliente }

constructor TServicoDeBuscaDeCliente.Create(const ARepositorioDeClientes: IRepositorioDeClientes);
begin
  FRepositorioDeClientes := ARepositorioDeClientes;
end;

function TServicoDeBuscaDeCliente.Buscar(const ACPFJ: TCPFJ): TCliente;
begin
  if not Assigned(ACPFJ) then
    raise EArgumentException.Create('O argumento "ACPFJ" n�o pode ser nulo.');
  Result := FRepositorioDeClientes.Buscar(ACPFJ);
  if not Assigned(Result) then
    raise EClienteNaoEncontrado.CreateFmt('Cliente %s n�o encontrado.', [ACPFJ.Formatar()]);
end;

end.
