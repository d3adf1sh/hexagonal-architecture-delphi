unit Aplicacao.Servicos.Cliente.ServicoDeConsultaDeClientes;

interface

uses
  System.SysUtils,
  System.Generics.Collections,
  Modelo.Entidades.Cliente,
  Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeConsultaDeClientes,
  Aplicacao.Portas.Saida.RepositorioDeClientes;

type
  //TServicoDeConsultaDeClientes
  TServicoDeConsultaDeClientes = class sealed(TInterfacedObject, ICasoDeUsoDeConsultaDeClientes)
  private
    FRepositorioDeClientes: IRepositorioDeClientes;
  public
    constructor Create(const ARepositorioDeClientes: IRepositorioDeClientes);
    function Consultar(const ANomeFantasia: string): TObjectList<TCliente>;
  end;

implementation

{ TServicoDeConsultaDeClientes }

constructor TServicoDeConsultaDeClientes.Create(const ARepositorioDeClientes: IRepositorioDeClientes);
begin
  FRepositorioDeClientes := ARepositorioDeClientes;
end;

function TServicoDeConsultaDeClientes.Consultar(const ANomeFantasia: string): TObjectList<TCliente>;
begin
  Result := FRepositorioDeClientes.Consultar(ANomeFantasia);
end;

end.
