unit Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeAlteracaoDeCliente;

interface

uses
  Modelo.Entidades.Cliente;

type
  //ICasoDeUsoDeAlteracaoDeCliente
  ICasoDeUsoDeAlteracaoDeCliente = interface
    ['{40221AEF-3F02-435E-A7D1-476597C98B5E}']
    function Alterar(const ACliente: TCliente): TCliente;
  end;

implementation

end.
