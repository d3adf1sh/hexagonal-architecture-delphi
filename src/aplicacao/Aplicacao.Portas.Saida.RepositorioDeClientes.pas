unit Aplicacao.Portas.Saida.RepositorioDeClientes;

interface

uses
  System.Generics.Collections,
  Modelo.Entidades.Cliente,
  Modelo.Valores.CPFJ;

type
  //IRepositorioDeClientes
  IRepositorioDeClientes = interface
    ['{5A200F1E-5EDA-4308-ACD8-FEF16ADEF400}']
    function Inserir(const ACliente: TCliente): TCliente;
    function Alterar(const ACliente: TCliente): TCliente;
    function Excluir(const ACPFJ: TCPFJ): TCliente;
    function Buscar(const ACPFJ: TCPFJ): TCliente;
    function Consultar(const ANomeFantasia: string): TObjectList<TCliente>;
  end;

implementation

end.
