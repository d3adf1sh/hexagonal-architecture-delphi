unit Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeConsultaDeClientes;

interface

uses
  System.Generics.Collections,
  Modelo.Entidades.Cliente;

type
  //ICasoDeUsoDeConsultaDeClientes
  ICasoDeUsoDeConsultaDeClientes = interface
    ['{4CB1FED1-1878-43C5-BBE4-CF56E80C1112}']
    function Consultar(const ANomeFantasia: string): TObjectList<TCliente>;
  end;

implementation

end.
