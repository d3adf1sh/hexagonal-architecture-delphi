unit Aplicacao.Servicos.Cliente.ServicoDeExclusaoDeCliente;

interface

uses
  System.SysUtils,
  Modelo.Entidades.Cliente,
  Modelo.Valores.CPFJ,
  Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeExclusaoDeCliente,
  Aplicacao.Portas.Saida.RepositorioDeClientes;

type
  //TServicoDeExclusaoDeCliente
  TServicoDeExclusaoDeCliente = class sealed(TInterfacedObject, ICasoDeUsoDeExclusaoDeCliente)
  private
    FRepositorioDeClientes: IRepositorioDeClientes;
  public
    constructor Create(const ARepositorioDeClientes: IRepositorioDeClientes);
    function Excluir(const ACPFJ: TCPFJ): TCliente;
  end;

implementation

uses
  Aplicacao.Portas.Entrada.Cliente.Erros;

{ TServicoDeExclusaoDeCliente }

constructor TServicoDeExclusaoDeCliente.Create(const ARepositorioDeClientes: IRepositorioDeClientes);
begin
  FRepositorioDeClientes := ARepositorioDeClientes;
end;

function TServicoDeExclusaoDeCliente.Excluir(const ACPFJ: TCPFJ): TCliente;
begin
  if not Assigned(ACPFJ) then
    raise EArgumentException.Create('O argumento "ACPFJ" n�o pode ser nulo.');
  Result := FRepositorioDeClientes.Excluir(ACPFJ);
  if not Assigned(Result) then
    raise EClienteNaoEncontrado.CreateFmt('Cliente %s n�o encontrado.', [ACPFJ.Formatar()]);
end;

end.
