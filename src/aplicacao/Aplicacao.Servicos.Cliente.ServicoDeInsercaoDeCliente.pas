unit Aplicacao.Servicos.Cliente.ServicoDeInsercaoDeCliente;

interface

uses
  System.SysUtils,
  Modelo.Entidades.Cliente,
  Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeInsercaoDeCliente,
  Aplicacao.Portas.Saida.RepositorioDeClientes;

type
  //TServicoDeInsercaoDeCliente
  TServicoDeInsercaoDeCliente = class sealed(TInterfacedObject, ICasoDeUsoDeInsercaoDeCliente)
  private
    FRepositorioDeClientes: IRepositorioDeClientes;
  public
    constructor Create(const ARepositorioDeClientes: IRepositorioDeClientes);
    function Inserir(const ACliente: TCliente): TCliente;
  end;

implementation

uses
  Aplicacao.Portas.Entrada.Cliente.Erros;

{ TServicoDeInsercaoDeCliente }

constructor TServicoDeInsercaoDeCliente.Create(const ARepositorioDeClientes: IRepositorioDeClientes);
begin
  FRepositorioDeClientes := ARepositorioDeClientes;
end;

function TServicoDeInsercaoDeCliente.Inserir(const ACliente: TCliente): TCliente;
var
  lClienteExistente: TCliente;
begin
  if not Assigned(ACliente) then
    raise EArgumentException.Create('O argumento "ACliente" n�o pode ser nulo.');
  lClienteExistente := FRepositorioDeClientes.Buscar(ACliente.CPFJ);
  if Assigned(lClienteExistente) then
    try
      raise EClienteJaCadastrado.CreateFmt('Cliente %s j� cadastrado.', [ACliente.CPFJ.Formatar()]);
    finally
      if Assigned(lClienteExistente) then
        FreeAndNil(lClienteExistente);
    end;
  Result := FRepositorioDeClientes.Inserir(ACliente);
end;

end.
