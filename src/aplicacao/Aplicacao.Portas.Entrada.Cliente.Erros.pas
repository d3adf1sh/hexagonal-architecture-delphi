unit Aplicacao.Portas.Entrada.Cliente.Erros;

interface

uses
  System.SysUtils;

type
  //EClienteJaCadastrado
  EClienteJaCadastrado = class(Exception);

  //EClienteNaoEncontrado
  EClienteNaoEncontrado = class(Exception);

implementation

end.
