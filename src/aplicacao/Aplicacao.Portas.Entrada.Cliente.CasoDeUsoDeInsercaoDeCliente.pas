unit Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeInsercaoDeCliente;

interface

uses
  Modelo.Entidades.Cliente;

type
  //ICasoDeUsoDeInsercaoDeCliente
  ICasoDeUsoDeInsercaoDeCliente = interface
    ['{19A1CC1A-A0B7-4B63-8A41-D5776EC192E4}']
    function Inserir(const ACliente: TCliente): TCliente;
  end;

implementation

end.
