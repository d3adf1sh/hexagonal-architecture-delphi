unit Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeExclusaoDeCliente;

interface

uses
  Modelo.Entidades.Cliente,
  Modelo.Valores.CPFJ;

type
  //ICasoDeUsoDeExclusaoDeCliente
  ICasoDeUsoDeExclusaoDeCliente = interface
    ['{4F439559-A3EF-4674-AE6B-3F2137B08367}']
    function Excluir(const ACPFJ: TCPFJ): TCliente;
  end;

implementation

end.
