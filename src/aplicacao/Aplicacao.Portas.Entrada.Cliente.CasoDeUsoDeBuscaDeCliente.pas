unit Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeBuscaDeCliente;

interface

uses
  Modelo.Entidades.Cliente,
  Modelo.Valores.CPFJ;

type
  //ICasoDeUsoDeBuscaDeCliente
  ICasoDeUsoDeBuscaDeCliente = interface
    ['{66B617A5-326F-4A6D-BC30-66D37A40DC38}']
    function Buscar(const ACPFJ: TCPFJ): TCliente;
  end;

implementation

end.
