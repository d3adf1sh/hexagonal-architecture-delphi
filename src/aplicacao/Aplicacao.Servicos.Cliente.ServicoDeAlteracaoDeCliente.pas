unit Aplicacao.Servicos.Cliente.ServicoDeAlteracaoDeCliente;

interface

uses
  System.SysUtils,
  Modelo.Entidades.Cliente,
  Aplicacao.Portas.Entrada.Cliente.CasoDeUsoDeAlteracaoDeCliente,
  Aplicacao.Portas.Saida.RepositorioDeClientes;

type
  //TServicoDeAlteracaoDeCliente
  TServicoDeAlteracaoDeCliente = class sealed(TInterfacedObject, ICasoDeUsoDeAlteracaoDeCliente)
  private
    FRepositorioDeClientes: IRepositorioDeClientes;
  public
    constructor Create(const ARepositorioDeClientes: IRepositorioDeClientes);
    function Alterar(const ACliente: TCliente): TCliente;
  end;

implementation

uses
  Aplicacao.Portas.Entrada.Cliente.Erros;

{ TServicoDeAlteracaoDeCliente }

constructor TServicoDeAlteracaoDeCliente.Create(const ARepositorioDeClientes: IRepositorioDeClientes);
begin
  FRepositorioDeClientes := ARepositorioDeClientes;
end;

function TServicoDeAlteracaoDeCliente.Alterar(const ACliente: TCliente): TCliente;
begin
  if not Assigned(ACliente) then
    raise EArgumentException.Create('O argumento "ACliente" n�o pode ser nulo.');
  Result := FRepositorioDeClientes.Alterar(ACliente);
  if not Assigned(Result) then
    raise EClienteNaoEncontrado.CreateFmt('Cliente %s n�o encontrado.', [ACliente.CPFJ.Formatar()]);
end;

end.
