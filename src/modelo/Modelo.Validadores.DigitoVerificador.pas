unit Modelo.Validadores.DigitoVerificador;

interface

uses
  System.SysUtils;

type
  //TDigitoVerificador
  TDigitoVerificador = class sealed
  private
    FPeso: TBytes;
    function Calcular(const ANumero: string): Integer;
  public
    constructor Create(const APeso: TBytes);
    function Valido(const ANumero: string): Boolean;
  end;

implementation

uses
  System.Math,
  Modelo.Rotinas.Texto;

{ TDigitoVerificador }

constructor TDigitoVerificador.Create(const APeso: TBytes);
begin
  if Length(APeso) = 0 then
    raise EArgumentException.Create('O argumento "APeso" deve ter ao menos um d�gito.');
  FPeso := APeso;
end;

function TDigitoVerificador.Valido(const ANumero: string): Boolean;
var
  nCaracteres: Integer;
  sNumeroBase: string;
  nPrimeiroDV: Integer;
  nSegundoDV: Integer;
begin
  nCaracteres := Length(FPeso) + 1;
  if Length(ANumero) <> nCaracteres then
    raise EArgumentException.CreateFmt('O argumento "ANumero" deve ter %d caracteres.', [nCaracteres]);
  if TTexto.CaracteresIguais(ANumero) then
    raise EArgumentException.Create('O argumento "ANumero" deve ser composto apenas por d�gitos.');
  sNumeroBase := Copy(ANumero, 1, Length(ANumero) - 2);
  nPrimeiroDV := Calcular(sNumeroBase);
  nSegundoDV := Calcular(sNumeroBase + IntToStr(nPrimeiroDV));
  Result := ANumero = sNumeroBase + IntToStr(nPrimeiroDV) + IntToStr(nSegundoDV);
end;

function TDigitoVerificador.Calcular(const ANumero: string): Integer;
var
  I: Integer;
  nSoma: Integer;
  nDigito: Integer;
begin
  I := Length(ANumero);
  nSoma := 0;
  while I > 0 do
  begin
    nDigito := StrToInt(Copy(ANumero, I, 1));
    nSoma := nSoma + (nDigito * FPeso[Length(FPeso) - Length(ANumero) + I - 1]);
    Dec(I);
  end;

  nSoma := 11 - nSoma mod 11;
  Result := IfThen(nSoma > 9, 0, nSoma);
end;

end.
