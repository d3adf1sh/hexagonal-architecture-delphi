unit Modelo.Rotinas.Texto;

interface

type
  //TTexto
  TTexto = class sealed
  public
    class function CaracteresIguais(const ATexto: string): Boolean;
    class function ExtrairNumeros(const ATexto: string): string;
  end;

implementation

uses
  System.SysUtils;

{ TTexto }

class function TTexto.CaracteresIguais(const ATexto: string): Boolean;
var
  bDiferenca: Boolean;
  sAnterior: Char;
  I: Integer;
begin
  bDiferenca := False;
  if Length(ATexto) > 1 then
  begin
    sAnterior := ATexto[1];
    I := 1;
    while (I <= Length(ATexto)) and not bDiferenca do
    begin
      if ATexto[I] <> sAnterior then
        bDiferenca := True
      else
      begin
        sAnterior := ATexto[I];
        Inc(I);
      end;
    end;
  end;

  Result := not bDiferenca;
end;

class function TTexto.ExtrairNumeros(const ATexto: string): string;
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Length(ATexto) do
    if CharInSet(ATexto[I], ['0'..'9']) then
      Result := Result + ATexto[I];
end;

end.
