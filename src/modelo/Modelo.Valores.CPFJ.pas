unit Modelo.Valores.CPFJ;

interface

type
  //TCPFJ
  TCPFJ = class sealed
  private
    FValor: string;
  public
    constructor Create(const AValor: string);
    property Valor: string read FValor;
    function Formatar: string;
    class function De(const ACPFJ: string): TCPFJ;
  end;

implementation

uses
  System.SysUtils,
  System.MaskUtils,
  Modelo.Validadores.DigitoVerificador,
  Modelo.Rotinas.Texto;

{ TCPFJ }

constructor TCPFJ.Create(const AValor: string);
var
  nTamanho: Integer;
  lDV: TDigitoVerificador;
begin
  nTamanho := Length(AValor);
  if not (nTamanho in [11..14]) then
    raise EArgumentException.Create('O argumento "AValor" deve ter 11 ou 14 d�gitos.');
  if TTexto.CaracteresIguais(AValor) then
    raise EArgumentException.Create('O argumento "AValor" n�o pode ter todos os d�gitos iguais.');
  if nTamanho = 11 then
    lDV := TDigitoVerificador.Create([11, 10, 9, 8, 7, 6, 5, 4, 3, 2])
  else
    lDV := TDigitoVerificador.Create([6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]);
  try
    if not lDV.Valido(AValor) then
      raise EArgumentException.Create('O argumento "AValor" deve ter d�gito verificador v�lido.');
  finally
    FreeAndNil(lDV);
  end;

  FValor := AValor;
end;

function TCPFJ.Formatar: string;
begin
  if Length(FValor) = 11 then
    Result := FormatMaskText('999.999.999-99;0', FValor)
  else
    Result := FormatMaskText('99.999.999/9999-99;0', FValor);
end;

class function TCPFJ.De(const ACPFJ: string): TCPFJ;
begin
  Result := TCPFJ.Create(TTexto.ExtrairNumeros(ACPFJ));
end;

end.
