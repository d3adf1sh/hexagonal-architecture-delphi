unit Modelo.Valores.Numero;

interface

type
  //TNumero
  TNumero = class sealed
  private
    FValor: string;
  public
    constructor Create(const AValor: string);
    property Valor: string read FValor;
    function Formatar: string;
    class function De(const ATelefone: string): TNumero;
  end;

implementation

uses
  System.SysUtils,
  System.MaskUtils,
  Modelo.Rotinas.Texto;

{ TNumero }

constructor TNumero.Create(const AValor: string);
begin
  if not (Length(AValor) in [10..11]) then
    raise EArgumentException.Create('O argumento "AValor" deve ter 10 ou 11 d�gitos.');
  FValor := AValor;
end;

function TNumero.Formatar: string;
begin
  if Length(FValor) = 10 then
    Result := FormatMaskText('(99)9999-9999;0', FValor)
  else
    Result := FormatMaskText('(99)99999-9999;0', FValor);
end;

class function TNumero.De(const ATelefone: string): TNumero;
begin
  Result := TNumero.Create(TTexto.ExtrairNumeros(ATelefone));
end;

end.
