unit Modelo.Entidades.Cliente;

interface

uses
  System.Generics.Collections,
  Modelo.Valores.CPFJ,
  Modelo.Valores.Numero;

type
  //Forward declarations
  TContato = class;
  TTelefone = class;

  //TCliente
  TCliente = class sealed
  private
    FNomeFantasia: string;
    FRazaoSocial: string;
    FCPFJ: TCPFJ;
    FContatos: TObjectList<TContato>;
  public
    constructor Create(const ANomeFantasia, ARazaoSocial: string; const ACPFJ: TCPFJ;
      const AContatos: TObjectList<TContato>);
    destructor Destroy; override;
    property NomeFantasia: string read FNomeFantasia;
    property RazaoSocial: string read FRazaoSocial;
    property CPFJ: TCPFJ read FCPFJ;
    property Contatos: TObjectList<TContato> read FContatos;
  end;

  //TContato
  TContato = class sealed
  private
    FNome: string;
    FTelefones: TObjectList<TTelefone>;
  public
    constructor Create(const ANome: string; const ATelefones: TObjectList<TTelefone>);
    destructor Destroy; override;
    property Nome: string read FNome;
    property Telefones: TObjectList<TTelefone> read FTelefones;
  end;

  //TTelefone
  TTelefone = class sealed
  private
    FDescricao: string;
    FNumero: TNumero;
  public
    constructor Create(const ADescricao: string; const ANumero: TNumero);
    destructor Destroy; override;
    property Descricao: string read FDescricao;
    property Numero: TNumero read FNumero;
  end;

implementation

uses
  System.SysUtils;

{ TCliente }

constructor TCliente.Create(const ANomeFantasia, ARazaoSocial: string; const ACPFJ: TCPFJ;
  const AContatos: TObjectList<TContato>);
begin
  if Length(ANomeFantasia) = 0 then
    raise EArgumentException.Create('O argumento "ANomeFantasia" n�o pode ser vazio.');
  if Length(ARazaoSocial) = 0 then
    raise EArgumentException.Create('O argumento "ARazaoSocial" n�o pode ser vazio.');
  if not Assigned(ACPFJ) then
    raise EArgumentException.Create('O argumento "ACPFJ" n�o pode ser nulo.');
  if not Assigned(AContatos) then
    raise EArgumentException.Create('O argumento "AContatos" n�o pode ser nulo.');
  if AContatos.Count = 0 then
    raise EArgumentException.Create('O argumento "AContatos" n�o pode ser vazio.');
  FNomeFantasia := ANomeFantasia;
  FRazaoSocial := ARazaoSocial;
  FCPFJ := ACPFJ;
  FContatos := AContatos;
end;

destructor TCliente.Destroy;
begin
  FreeAndNil(FContatos);
  FreeAndNil(FCPFJ);
  inherited;
end;

{ TContato }

constructor TContato.Create(const ANome: string; const ATelefones: TObjectList<TTelefone>);
begin
  if Length(ANome) = 0 then
    raise EArgumentException.Create('O argumento "ANome" n�o pode ser vazio.');
  if not Assigned(ATelefones) then
    raise EArgumentException.Create('O argumento "ATelefones" n�o pode ser nulo.');
  if ATelefones.Count = 0 then
    raise EArgumentException.Create('O argumento "ATelefones" n�o pode ser vazio.');
  FNome := ANome;
  FTelefones := ATelefones;
end;

destructor TContato.Destroy;
begin
  FreeAndNil(FTelefones);
  inherited;
end;

{ TTelefone }

constructor TTelefone.Create(const ADescricao: string; const ANumero: TNumero);
begin
  if Length(ADescricao) = 0 then
    raise EArgumentException.Create('O argumento "ADescricao" n�o pode ser vazio.');
  if not Assigned(ANumero) then
    raise EArgumentException.Create('O argumento "ANumero" n�o pode ser nulo.');
  FDescricao := ADescricao;
  FNumero := ANumero;
end;

destructor TTelefone.Destroy;
begin
  FreeAndNil(FNumero);
  inherited;
end;

end.
