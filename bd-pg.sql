-- drops
DROP TABLE IF EXISTS public.telefone;
DROP TABLE IF EXISTS public.contato;
DROP TABLE IF EXISTS public.cliente;

-- cliente
CREATE TABLE public.cliente (
    id_cliente bigint NOT NULL,
    nome_fantasia character varying(255),
    razao_social character varying(255),
    cpfj character varying(255),
    data_de_insercao timestamp(6) without time zone,
    data_de_alteracao timestamp(6) without time zone,
    CONSTRAINT pk_cliente PRIMARY KEY (id_cliente),
    CONSTRAINT uk_cliente unique (cpfj)
);

CREATE SEQUENCE public.seq_cliente_id_cliente;
ALTER SEQUENCE public.seq_cliente_id_cliente OWNED BY public.cliente.id_cliente;
ALTER TABLE public.cliente ALTER COLUMN id_cliente SET DEFAULT nextval('public.seq_cliente_id_cliente'::regclass);

-- contato
CREATE TABLE public.contato (
    id_contato bigint NOT NULL,
    id_cliente bigint,
    nome character varying(255),
    CONSTRAINT pk_contato PRIMARY KEY (id_contato),
   	CONSTRAINT fk_contato_cliente FOREIGN KEY (id_cliente) REFERENCES public.cliente(id_cliente)
);

CREATE SEQUENCE public.seq_contato_id_contato;
ALTER SEQUENCE public.seq_contato_id_contato OWNED BY public.contato.id_contato;
ALTER TABLE public.contato ALTER COLUMN id_contato SET DEFAULT nextval('public.seq_contato_id_contato'::regclass);

-- telefone
CREATE TABLE public.telefone (
    id_telefone bigint NOT NULL,
    id_contato bigint,
    descricao character varying(255),
    numero character varying(255),
    CONSTRAINT pk_telefone PRIMARY KEY (id_telefone),
    CONSTRAINT fk_telefone_contato FOREIGN KEY (id_contato) REFERENCES public.contato(id_contato)
);

CREATE SEQUENCE public.seq_telefone_id_telefone;
ALTER SEQUENCE public.seq_telefone_id_telefone OWNED BY public.telefone.id_telefone;
ALTER TABLE public.telefone ALTER COLUMN id_telefone SET DEFAULT nextval('public.seq_telefone_id_telefone'::regclass);