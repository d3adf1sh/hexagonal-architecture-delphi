-- drops
DROP TABLE IF EXISTS `Telefone`;
DROP TABLE IF EXISTS `Contato`;
DROP TABLE IF EXISTS `Cliente`;

-- `Cliente`
CREATE TABLE `Cliente` (
  `idCliente` bigint NOT NULL AUTO_INCREMENT,
  `nomeFantasia` varchar(255) DEFAULT NULL,
  `razaoSocial` varchar(255) DEFAULT NULL,
  `cpfj` varchar(255) DEFAULT NULL,
  `dataDeInsercao` datetime(6) DEFAULT NULL,
  `dataDeAlteracao` datetime(6) DEFAULT NULL,
  constraint `PK_Cliente` PRIMARY KEY (`idCliente`),
  constraint `UK_Cliente` UNIQUE KEY (`cpfj`)
) ENGINE=InnoDB;

-- `Contato`
CREATE TABLE `Contato` (
  `idContato` bigint NOT NULL AUTO_INCREMENT,
  `idCliente` bigint DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  constraint `PK_Contato` PRIMARY KEY (`idContato`),
  CONSTRAINT `FK_Contato_Cliente` FOREIGN KEY (`idCliente`) REFERENCES `Cliente` (`idCliente`)
) ENGINE=InnoDB;

-- `Telefone`
CREATE TABLE `Telefone` (
  `idTelefone` bigint NOT NULL AUTO_INCREMENT,
  `idContato` bigint DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `numero` varchar(255) DEFAULT NULL,
  constraint `PK_Telefone` PRIMARY KEY (`idTelefone`),
  CONSTRAINT `FK_Telefone_Contato` FOREIGN KEY (`idContato`) REFERENCES `Contato` (`idContato`)
) ENGINE=InnoDB;