﻿# To do

- Executar os bancos de dados em contêineres;
- Converter modelos através de um mapeador;
- Pool de conexões de banco de dados;
- Usar listas imutáveis no modelo;
- Consolidar `modelo` e `aplicacao` em um único pacote com os namespaces `entidades`, `portas` e `servicos`;
- Remover o termo **Argumento** nas validações;
- Testar outras formas de validação (Notifications/Either, método público, factory, etc);
- Mover os adaptadores de entrada (`REST`) para o namespace `Recursos` ou `PontosDeEntrada`;
- Usar um repositório por operação;
- Iniciar os atributos dos JSONs em minúsculo;
- Remover o atributo `data` dos JSONs;
- Conectar no MongoDB sem passar pelo FireDAC;
- Ajustar a **time-zone** para que os campos de data e hora não sejam persistidos com horas a mais no MongoDB;
- Indexar a busca por CPFJ no MongoDB;
- Implementar adaptadores para VCL e serviço externo;
- Renomear o pacote `bootstrap` para `server` ou `configuration`;
- TDD.
